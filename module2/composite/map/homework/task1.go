package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},{
			Name:  "https://github.com/docker/buildx",
			Stars: 2312,
		},{
			Name:  "https://github.com/docker/compose-cli",
			Stars: 888,
		},{
			Name:  "https://github.com/docker/docker-credential-helpers",
			Stars: 831,
		},{
			Name:  "https://github.com/docker/docker-ce",
			Stars: 5567,
		},{
			Name:  "https://github.com/docker/engine",
			Stars: 734,
		},{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},{
			Name:  "https://github.com/docker/machine",
			Stars: 6545,
		},{
			Name:  "https://github.com/docker/go-plugins-helpers",
			Stars: 309,
		},{
			Name:  "https://github.com/docker/app",
			Stars: 1583,
		},{
			Name:  "https://github.com/docker/libchan",
			Stars: 2457,
		},{
			Name:  "https://github.com/docker/engine-api",
			Stars: 267,
		},{
			Name:  "https://github.com/docker/libtrust",
			Stars: 107,
		},
	}

	m := make(map[string]int)
	for i := 0; i < len(projects); i++{
		m[projects[i].Name] = projects[i].Stars
	}

	for value := range m {
		fmt.Println("Value:", value)
	}
}

// в комментриях напишите пожалуйста, хотя бы намекните что не правильно,
//потому что я не понимаю по заданию я сделал не так
