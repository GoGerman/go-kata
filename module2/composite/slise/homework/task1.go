package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = Append1(s) // первое решение
	//Append2(&s)
	// второе решение

	// s = append(s, 4)
	// третье решение
	fmt.Println(s)
}

func Append1(s []int) []int {
	s = append(s, 4)
	return s
}

//func Append2(s *[]int) {
//	*s = append(*s, 4)
//}
