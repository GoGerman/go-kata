package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}

	subUsers := users[2:len(users)]
	editedSlise := editSecondSlice(subUsers)

	fmt.Println(editedSlise)
	fmt.Println(users)
}

func editSecondSlice(users []User) []User {
	editedSlise := []User{}
	for i := range users {
		editedSlise = append(editedSlise, users[i])
		editedSlise[i].Name = "unknown"
	}
	return editedSlise
}
