package main

import "fmt"

func main() {
	testArray()
}

func testArray() {
	a := [...]int{34,52, 89, 105}
	fmt.Println("original value:", a)
	a[0] = 21
	fmt.Println("changed first element", a)
	b := a
	a[0] = 233
	fmt.Println("original array", a)
	fmt.Println("modified array", b)
}
