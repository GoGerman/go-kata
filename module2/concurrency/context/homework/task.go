package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func main() {

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	ctx := context.Background()

	go func() {
		withDeadLine, cancel := context.WithDeadline(ctx, time.Now().Add(30*time.Second))
		defer cancel()
		for _, num := range []int{1, 2, 3} {
			a <- num
		}
		select {
		case <-withDeadLine.Done():
			close(a)
		default:
			return
		}
	}()

	go func() {
		withDeadLine, cancel := context.WithDeadline(ctx, time.Now().Add(30*time.Second))
		defer cancel()
		for _, num := range []int{20, 10, 30} {
			b <- num
		}
		select {
		case <-withDeadLine.Done():
			close(b)
		default:
			return
		}
	}()

	go func() {
		withDeadLine, cancel := context.WithDeadline(ctx, time.Now().Add(30*time.Second))
		defer cancel()
		for _, num := range []int{300, 200, 100} {
			c <- num
		}
		select {
		case <-withDeadLine.Done():
			close(c)
		default:
			return
		}
	}()

	ticker := time.NewTicker(time.Second)
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				fmt.Println("Tick at", t)
			}
		}
	}()

	timeToSleep()

	ticker.Stop()
	done <- true
	fmt.Println("Ticker stopped")

	for num := range joinChannels(a, b, c) {
		fmt.Println(num)
	}

}

func timeToSleep() {
	time.Sleep(30 * time.Second)
}
