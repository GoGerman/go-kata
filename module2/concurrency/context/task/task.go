package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

const (
	userIDctx key = 0
)

type key int

func main() {
	//ctx := context.Background()

	//ctx := context.WithValue(context.Background(), userIDctx, 1)
	//ctx, cancel := context.WithCancel(ctx)
	//
	//go func() {
	//	_, _ = fmt.Scanln()
	//	cancel()
	//}()
	//processLongTask(ctx)

	http.HandleFunc("/", handle)
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func processLongTask(ctx context.Context) string {
	id := ctx.Value(userIDctx)

	select {
	case <-time.After(2 * time.Second):
		return fmt.Sprintf("Done processing id: %d\n", id)
	case <-ctx.Done():
		log.Println("request cancelled")
		return ""
	}
}

func handle(w http.ResponseWriter, r *http.Request) {
	id := r.Header.Get("User-Id")

	ctx := context.WithValue(r.Context(), userIDctx, id)

	result := processLongTask(ctx)

	_, _ = w.Write([]byte(result))
}

//func processLongTask(ctx context.Context) {
//	id := ctx.Value(userIDctx)
//
//	select {
//	case <-time.After(2 * time.Second):
//		fmt.Println("Done processing id:", id)
//	case <-ctx.Done():
//		fmt.Println("request cancelled")
//	}
//}
