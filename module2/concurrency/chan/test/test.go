package main

import "fmt"

func main() {
	ch := make(chan int)

	go func(ch chan int) {
		ch <- 1
		ch <- 2
		ch <- 3
	}(ch)

	in := <-ch
	fmt.Println(in)

	in = <-ch
	fmt.Println(in)

	in = <-ch
	fmt.Println(in)
}
