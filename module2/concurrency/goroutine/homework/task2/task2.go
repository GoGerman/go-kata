package main

import (
	"fmt"
	"time"
)

func main() {
	message := make(chan string)

	//go func() {
	//	time.Sleep(2 * time.Second)
	//	message <- "hello"
	//}()
	//msg := <-message
	//fmt.Println(msg)
	//fmt.Println(<-message)

	go func() {
		for i := 1; i <= 10; i++ {
			message <- fmt.Sprintf("%d", i)
			time.Sleep(time.Millisecond * 250)
		}
		close(message)
	}()

	//for {
	//	msg, open := <-message
	//
	//	if !open {
	//		break
	//	}
	//	fmt.Println(msg)
	//}

	for i := range message {
		fmt.Println(i)
	}
}
