package main

import (
	"fmt"
	"net/http"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	urls := []string{
		"https://google.com/",
		"https://youtube.com/",
		"https://github.com/",
		"https://vk.com/",
		"https://medium.com/",
	}

	for _, url := range urls {
		wg.Add(1)
		go func(url string) {
			doHTTP(url)
			wg.Done()
		}(url)
	}

	wg.Wait()
}

func doHTTP(url string) {
	t := time.Now()

	resp, err := http.Get(url)
	check(err)

	defer resp.Body.Close()

	fmt.Printf("<%s> - Status code [%d] - Latency %d ms\n",
		url, resp.StatusCode, time.Since(t).Milliseconds())
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
