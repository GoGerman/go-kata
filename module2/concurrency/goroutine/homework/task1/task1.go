package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var w sync.WaitGroup

func main() {
	//go fmt.Println("Hello from goroutine")
	//go fmt.Println("Hello from main()")

	t := time.Now()
	rand.Seed(t.UnixNano())

	w.Add(2)
	go parseURL("http://example.com/")
	go parseURL("http://youtube.com/")
	w.Wait()

	fmt.Printf("Parsing completed. Time Elapesed: %.2f seconds\n", time.Since(t).Seconds())

	//time.Sleep(time.Millisecond)
}

func parseURL(url string) {
	for i := 0; i < 5; i++ {
		latency := rand.Intn((500) + 500)
		time.Sleep(time.Duration(latency) * time.Millisecond)
		fmt.Printf("Parsing <%s> - Step %d - Latency %d\n", url, i+1, latency)
	}
	w.Done()
}
