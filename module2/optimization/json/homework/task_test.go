package main

import (
	"os"
	"testing"
)

func BenchmarkUnmarshalTask(b *testing.B) {
	var (
		task Task
		err  error
		data []byte
	)
	jsonData, _ := os.ReadFile("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/optimization/json/homework/task.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		task, err = UnmarshalTask(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = task.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkUnmarshalTask2(b *testing.B) {
	var (
		task Task
		err  error
		data []byte
	)
	jsonData, _ := os.ReadFile("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/optimization/json/homework/task.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		task, err = UnmarshalTask2(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = task.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
