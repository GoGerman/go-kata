package main

import (
	"encoding/json"
	"fmt"
	"os"

	jsoiner "github.com/json-iterator/go"
)

type Task []TaskElement

func UnmarshalTask(data []byte) (Task, error) {
	var r Task
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Task) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalTask2(data []byte) (Task, error) {
	var r Task
	err := jsoiner.Unmarshal(data, &r)
	return r, err
}

func (r *Task) Marshal2() ([]byte, error) {
	return jsoiner.Marshal(r)
}

type TaskElement struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Status string

const (
	Pending Status = "pending"
)

func main() {
	file, _ := os.ReadFile("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/optimization/json/homework/task.json")
	jsonData, _ := UnmarshalTask(file)
	_, _ = jsonData.Marshal()
	fmt.Println(jsonData)

	jsonData1, _ := UnmarshalTask2(file)
	_, _ = jsonData1.Marshal2()
	fmt.Println(jsonData1)
}
