package main

import (
	"bytes"
	"fmt"
	"os"
)

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	file, err := os.Create("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/stl/io/homework/example.txt")
	check(err)
	var txt bytes.Buffer
	for i := range data {
		_, err1 := txt.WriteString(data[i] + "\n")
		check(err1)
		_, err = file.WriteString(txt.String())
		txt.Reset()
	}
	var txtAfter bytes.Buffer
	txt2, err := os.ReadFile("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/stl/io/homework/example.txt")
	txtAfter.WriteString(string(txt2))
	check(err)
	fmt.Println(txtAfter.String())
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
