package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app"`
	Production bool   `json:"Product"`
}

func main() {
	var conf Config

	config := flag.Bool("conf", true, "")
	flag.Parse()

	if filename := flag.Arg(0); filename != "" {
		file, err := os.ReadFile(filename)
		check(err)
		err = json.Unmarshal(file, &conf)
		check(err)
	}

	if *config {
		fmt.Printf("AppName: %v\nProduction: %v", conf.AppName, conf.Production)
	}

}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
