package main

import (
	"fmt"
	"strconv"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func main() {
	p := Person{Name: "Andy", Age: 18, Money: 832131}

	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)

	str := generateSelfStory(p.Name, p.Age, p.Money)
	fmt.Printf(str)
}

func generateSelfStory(name string, age int, money float64) string {
	s := fmt.Sprintf("%f", money)
	zap := "I'm "
	str := "Hello! My name is " + name + ". " + zap + strconv.Itoa(age) + " y.o. And I also have $" + s + " in my wallet right now."
	return str
}
