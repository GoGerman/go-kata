package main

import (
	"bufio"
	"fmt"
	"github.com/essentialkaos/translit/v2"
	"os"
)

func main() {
	text, err := os.ReadFile("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/stl/files/write/example.txt")
	check(err)
	data := string(text)
	data = translit.EncodeToICAO(data)
	file, err := os.Create("/Users/pusichkagerman/go/src/GoKata/go-kata/module2/stl/files/write/example.processed.txt")
	check(err)
	_, err = file.WriteString(data)
	check(err)
}

func testGreet() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter your name: ")
	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
	fmt.Println("Success")
}
