package main

import (
	"fmt"
	"gitlab.com/GoGerman/greet/greet" // почему то все подчеркивается красным но работает, не понимаю причину
	// cнимок экрана положил в папку
	//"greet/greet" // так работает когда я локально скачиваю пакет
)

func main() {
	greet.Hello()
	fmt.Println(greet.Shark)

	oct := greet.Octopus{
		Name:  "Jesse",
		Color: "green",
	}

	fmt.Println(oct.String())

	oct.Reset()

	fmt.Println(oct.String())
}