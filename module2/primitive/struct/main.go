package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Location Location
}

type Wallet struct {
	RUB, USD, BTC, ETH uint64
}

type Location struct {
	Address string
	City    string
	Index   string
}

func main() {
	wallet := Wallet{RUB: 250000, USD: 3500, BTC: 1, ETH: 4}
	user := User{Age: 13, Name: "Alexander", Wallet: wallet}
	fmt.Println(wallet)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")
	fmt.Println(user)
	user2 := User{Age: 34, Name: "Anton",
		Wallet: Wallet{
			RUB: 144000, USD: 3000, BTC: 30, ETH: 10},
		Location: Location{
			Address: "ул Спортивная, д 27, к 2", City: "Москва", Index: "144001"},
	}
	fmt.Println(user2)
}
