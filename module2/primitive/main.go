package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 11235813234
	fmt.Println(unsafe.Sizeof(n))
}
