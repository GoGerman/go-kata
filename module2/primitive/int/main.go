package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var uintNumber uint8 = 1 << 7
	var from = int8(uintNumber)
	var from2 = int16(uintNumber)
	uintNumber--
	var to = int8(uintNumber)
	var to2 = int16(uintNumber)
	fmt.Println(from, to)
	fmt.Println(from2, to2)
	typeInt()
}

func typeInt()  {
	fmt.Println("=== START type uint ===")
	var numberInt8 int8 = 1 << 1
	fmt.Println("left shift int8:", numberInt8, "size:", unsafe.Sizeof(numberInt8))
	numberInt8 = (1 << 7) - 1
	fmt.Println("int8 max value:", numberInt8, "size:", unsafe.Sizeof(numberInt8))
	var numberInt16 int16 = (1 << 15) - 1
	fmt.Println("int16 max value:", numberInt16, "size:", unsafe.Sizeof(numberInt16))
	var numberInt32 int32 = (1 << 31) - 1
	fmt.Println("int32 max value:", numberInt32, "size:", unsafe.Sizeof(numberInt32))
	var numberInt64 int64 = (1 << 63) - 1
	fmt.Println("int64 max value:", numberInt64, "size:", unsafe.Sizeof(numberInt64))
	fmt.Println("=== End type uint ===")
}
