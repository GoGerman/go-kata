package solution

import "errors"

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error)  {
	defer func() {
		job.IsFinished = true
	}()

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}

	for _, i := range job.Dicts {
		if i == nil {
			return job, errNilDict
		}
		for l, j := range i {
			job.Merged[l] = j
		}
	}
	return job, nil
}
