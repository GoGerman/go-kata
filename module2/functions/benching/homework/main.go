package main

import (
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"sort"
)

type User struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product
}

type Product struct {
	UserID int64
	Name   string `fake:"{sentence:3}"`
}

func main() {
	users := genUsers()
	products := genProducts()
	//fmt.Println(products, "\n")
	//users = MapUserProducts(users, products)
	m := make(map[int64][]Product)
	m = MapUserProducts2(users, products)
	fmt.Println(m)
	//fmt.Println(users)
}

func MapUserProducts(users []User, products []Product) []User {
	// Проинициализируйте карту продуктов по айди пользователей
	for i, user := range users {
		for _, product := range products { // избавьтесь от цикла в цикле
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}

	return users
}

func MapUserProducts2(users []User, products []Product) map[int64][]Product {
	m := make(map[int64][]Product)
	for _, product := range products {
		m[users[product.UserID].ID] = append(m[product.UserID], product)
	}

	return m
}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserID = int64(rand.Intn(100))
		products[i] = product
	}

	sort.Slice(products, func(i, j int) (less bool) {
		return products[i].UserID < products[j].UserID
	})

	return products
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		_ = gofakeit.Struct(&user)
		user.ID = int64(i)
		users[i] = user
	}

	return users
}
