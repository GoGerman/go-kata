package main

import (
	"testing"
)

func BenchmarkSlice(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMap(b *testing.B) {
	users := genUsers()
	products := genProducts()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
}
