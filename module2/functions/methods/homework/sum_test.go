package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSum(t *testing.T) {
	type testCase struct {
		a, b, result float64
	}

	testCases := []testCase{
		{a: 122, b: 12, result: 122 + 12.0},
		{a: 23, b: 45, result: 23 + 45.0},
		{a: 65, b: 33, result: 65 + 33.0},
		{a: 12, b: 67, result: 12 + 67.0},
		{a: 2, b: 8, result: 2 + 8.0},
		{a: 98, b: 30, result: 98 + 30.0},
		{a: 65, b: 72, result: 65 + 72.0},
		{a: 87, b: 11, result: 87 + 11.0},
	}

	calc := NewCalc()

	for _, test := range testCases {
		t.Run("Sum", func(t *testing.T) {
			assert.Equalf(t, test.result, calc.SetA(test.a).SetB(test.b).Do(sum).Result(), "%v, %v", test.a, test.b)
		})

	}
}
