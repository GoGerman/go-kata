package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestAverage(t *testing.T) {
	type testCase struct {
		a, b, result float64
	}

	testCases := []testCase{
		{a: 122, b: 12, result: 67},
		{a: 23, b: 45, result: 34},
		{a: 65, b: 33, result: 49},
		{a: 12, b: 67, result: 39.5},
		{a: 2, b: 8, result: 5},
		{a: 98, b: 30, result: 64},
		{a: 65, b: 72, result: 68.5},
		{a: 87, b: 11, result: 49},
	}

	calc := NewCalc()

	for _, test := range testCases {
		t.Run("Average", func(t *testing.T) {
			assert.Equalf(t, test.result, calc.SetA(test.a).SetB(test.b).Do(average).Result(), "%v, %v", test.a, test.b)
		})

	}
}
