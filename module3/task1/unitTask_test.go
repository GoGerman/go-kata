package main

import (
	"errors"
	"github.com/stretchr/testify/require"
	"testing"
	"time"
)

var (
	Error    = errors.New("Ошибка")
	DoubleLL = testData()
)

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	cases := []struct {
		name  string
		d     *DoubleLinkedList
		error error
	}{
		{
			name:  "List is empty",
			d:     DoubleLL,
			error: Error,
		},
		{
			name:  "List is allowed",
			d:     DoubleLL,
			error: nil,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.d.DeleteCurrent()
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.error, err.Error())
			} else {
				require.NoError(t, err)
			}
		})
	}

}

func TestDoubleLinkedList_Delete(t *testing.T) {
	cases := []struct {
		name  string
		d     *DoubleLinkedList
		n     int
		error error
	}{
		{
			name:  "deleted",
			d:     DoubleLL,
			n:     4,
			error: Error,
		},
		{
			name:  "deleted",
			d:     DoubleLL,
			n:     -1,
			error: Error,
		},
		{
			name:  "deleted",
			d:     DoubleLL,
			n:     0,
			error: nil,
		},
		{
			name:  "List is empty",
			d:     DoubleLL,
			n:     3,
			error: Error,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.d.Delete(tCase.n)
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.error, err.Error())
			} else {
				require.NoError(t, err)
			}

		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	cases := []struct {
		name  string
		d     *DoubleLinkedList
		n     int
		error error
	}{
		{
			name:  "Add 1 elem in 1pos",
			d:     DoubleLL,
			n:     1,
			error: nil,
		},
		{
			name:  "Add 1 elem in 2pos",
			d:     DoubleLL,
			n:     2,
			error: nil,
		},
		{
			name:  "Add 1 elem in 3pos",
			d:     DoubleLL,
			n:     3,
			error: nil,
		},
		{
			name:  "Add 1 elem when n < 0",
			d:     DoubleLL,
			n:     -3,
			error: Error,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.d.Insert(tCase.n, Commit{
				Message: "add new commit",
				Date:    time.Date(2012, time.October, 11, 12, 30, 20, 0, time.UTC),
				UUID:    "1236d3f4-875b-11ed-8150-acde48001122"})
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.error, err.Error())
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestDoubleLinkedList_Index_error(t *testing.T) {
	cases := []struct {
		name  string
		d     *DoubleLinkedList
		error error
	}{
		{
			name:  "List is empty",
			d:     DoubleLL,
			error: Error,
		},
		{
			name:  "List is allowed",
			d:     DoubleLL,
			error: nil,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			_, err := tCase.d.Index()
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.error, err.Error())
			} else {
				require.NoError(t, err)
			}

		})
	}

}

func TestDoubleLinkedList_Index_ReturnInt(t *testing.T) {
	cases := []struct {
		name string
		d    *DoubleLinkedList
		err  error
	}{
		{
			name: "List is empty",
			d:    DoubleLL,
			err:  Error,
		},
		{
			name: "List is allowed",
			d:    DoubleLL,
			err:  Error,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			_, err := tCase.d.Index()
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.err, err.Error())
			}
		})
	}
}
