package main

import (
	"encoding/json"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"os"
	"time"
)

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	Append(c Commit)
	LoadData(path string) error
	Insert(n int, c Commit) error
	GetNode(index int) *Node
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func (d *DoubleLinkedList) LoadData(path string) error {
	file, _ := os.ReadFile(path)
	var commits []Commit
	err := json.Unmarshal(file, &commits)
	commits1 := QuickSortCommitsDate(commits)
	for _, commit := range commits1 {
		d.Append(commit)
	}
	return err
}

func (d *DoubleLinkedList) Append(commit Commit) {
	node := &Node{
		data: &commit,
	}
	if d.len == 0 {
		d.head = node
		d.tail = node
		d.curr = node
	} else {
		d.tail.next = node
		node.prev = d.tail
		d.tail = node
		d.curr = node
	}
	d.len++
}

func QuickSortCommitsDate(commits []Commit) []Commit {
	if len(commits) <= 1 {
		return commits
	}

	median := commits[len(commits)/2].Date

	lowPart := make([]Commit, 0, len(commits))
	middlePart := make([]Commit, 0, len(commits))
	highPart := make([]Commit, 0, len(commits))

	for _, commit := range commits {
		switch {
		case commit.Date.Before(median):
			lowPart = append(lowPart, commit)
		case commit.Date.After(median):
			highPart = append(highPart, commit)
		case commit.Date.Equal(median):
			middlePart = append(middlePart, commit)
		}
	}

	lowPart = QuickSortCommitsDate(lowPart)
	highPart = QuickSortCommitsDate(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.head == nil || d.curr.next == nil {
		return nil
	}
	defer func() {
		d.curr = d.curr.prev
	}()
	return d.curr.next
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.head == nil || d.curr.prev == nil {
		return nil
	}
	defer func() {
		d.curr = d.curr.prev
	}()
	return d.curr.prev
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	node := &Node{
		data: &c,
	}
	if n == 0 {
		node.next = d.head
		d.head.prev = node
		d.head = node
		d.len++
		return nil
	}
	var err error
	if n < 0 {
		return err
	}

	if n >= d.len {
		d.Append(*node.data)
		return nil
	} else {

		oldNode := d.GetNode(n)

		node.prev = oldNode.prev
		node.next = oldNode

		oldNode.prev.next = node
		oldNode.prev = node

		d.len++
	}
	return nil
}

func (d *DoubleLinkedList) GetNode(index int) *Node {
	if d.len == 0 || index >= d.len {
		return nil
	}

	if index == 0 {
		return d.head
	}

	node := d.head
	for i := 0; i < index; i++ {
		node = d.head.next
	}
	return node
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	idx, err := d.Index()
	if err != nil {
		return err
	}

	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}

	err = d.DeleteCurrent()
	return err
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	switch {
	case d.head == nil:
		var err error
		return err
	case d.head == d.curr:
		d.head = d.head.next
		d.curr = d.head
		d.len--
		return nil
	case d.tail == d.curr:
		d.tail.prev.next = nil
		d.tail = d.tail.prev
		d.curr = d.tail
		d.len--
		return nil
	case d.curr == nil:
		var err error
		return err
	default:
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.curr = d.curr.next
		d.len--
		return nil
	}
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	var err error
	if d.head == nil {
		return -1, err
	}
	if d.len == 0 {
		return -1, err
	}

	var prevNode *Node
	var number int
	currNode := d.head
	for currNode != d.curr {
		prevNode = currNode
		currNode = prevNode.next
		number++
	}
	return number, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		return nil
	}

	//data := d.head.data
	d.head = d.head.next
	if d.head != nil {
		d.head.prev = nil
	} else {
		d.tail = nil
	}
	d.len -= 1
	return d.head
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		return nil
	}

	d.tail = d.tail.prev
	if d.tail != nil {
		d.tail.next = nil
	} else {
		d.head = nil
	}
	d.len--
	return d.tail
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	for d.head != nil {
		if d.head.data.UUID == uuID {
			return d.head
		}
		if d.head == d.tail {
			fmt.Println("Not found")
			return &Node{}
		}
		d.head = d.head.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	for d.head != nil {
		if d.head.data.Message == message {
			return d.head
		}
		if d.head == d.tail {
			fmt.Println("Not found")
			return &Node{}
		}
		d.head = d.head.next
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d.head == nil {
		return nil
	}

	d.head, d.tail = d.tail, d.head
	for d.head != nil {
		next := d.head.next
		d.head.next, d.head.prev = d.head.prev, d.head.next
		d.head = next
	}
	return d
}

func GenerateJSON() (string, int) {
	rand.Seed(time.Now().UnixNano())
	n := rand.Intn(100)
	var commits = make([]Commit, n)
	for i, commit := range commits {
		_ = gofakeit.Struct(&commit)
		commits[i] = commit
	}
	j, err := json.Marshal(commits)
	if err != nil {
		panic(err)
	}
	return string(j), n
}

func main() {
	file := "/Users/pusichkagerman/go/src/GoKata/go-kata/module3/task1/test.json"
	data := &DoubleLinkedList{}
	_ = data.LoadData(file)

	_ = data.Delete(3)
}
