package main

import (
	"testing"
	"time"
)

var File = "/Users/pusichkagerman/go/src/GoKata/go-kata/module3/task1/test.json"

func testData() *DoubleLinkedList {
	d := DoubleLinkedList{}
	_ = d.LoadData(File)
	return &d
}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {
	list := testData()
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		if err := list.LoadData(File); err != nil {
			panic(err)
		}
	}
}

func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Len()
	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Prev()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Next()
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	list := testData()
	newNode := &Commit{"AAAAAA",
		"BBBBB",
		time.Now()}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = list.Insert(10, *newNode)
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < 1999; i++ {
		_ = list.Delete(1)
	}
}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < 1999; i++ {
		_ = list.DeleteCurrent()
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = list.Index()
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Current()
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < 1999; i++ {
		list.Pop()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < 1999; i++ {
		list.Shift()
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.SearchUUID(list.head.data.UUID)
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Search(list.head.data.Message)
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	list := testData()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		list.Reverse()
	}
}
