package repo

import (
	"encoding/json"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/model"
	"log"
	"os"
)

// TaskRepository is a repositories interface for tasks
type TaskRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) (model.Todo, error)
	UpdateTask(task model.Todo) (model.Todo, error)
	DeleteTask(id int) error
	UpdateForService(task model.Todo) (model.Todo, error)
	SaveTasksForService(tasks []model.Todo) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func NewFileTaskRepository(file string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: file}
}

func (repo *FileTaskRepository) UpdateForService(task model.Todo) (model.Todo, error) {
	tasks, err := repo.GetTasks()
	checkError(err)

	for i, t := range tasks {
		if t.ID == task.ID {
			task.Status = "done"
			tasks[i] = task
			t.Status = "done"
			err = repo.SaveTasksForService(tasks)
			checkError(err)
			return task, nil
		}
	}
	var err1 error
	return task, err1
}

func (repo *FileTaskRepository) SaveTasksForService(tasks []model.Todo) error {
	_, err := os.OpenFile(repo.FilePath, os.O_TRUNC, 0644)
	checkError(err)

	var newTasks []model.Todo
	newTasks = append(newTasks, tasks...)

	txt, err := json.Marshal(newTasks)
	checkError(err)

	err = os.WriteFile(repo.FilePath, txt, 0644)
	checkError(err)

	return nil
}

// CreateTask adds a new task to the repositories
func (repo *FileTaskRepository) CreateTask(task model.Todo) (model.Todo, error) {
	var tasks []model.Todo

	lenTasks, err := repo.GetTasks()
	checkError(err)

	task.ID = len(lenTasks) + 1
	tasks = append(tasks, task)

	err = repo.SaveTasks(tasks)
	checkError(err)

	return task, nil
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	checkError(err)

	for i, task := range tasks {
		if task.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
	}

	file, err := os.Open(repo.FilePath)
	checkError(err)

	_, err = file.Seek(0, 0)
	checkError(err)

	for i, task := range tasks {
		task.ID = i + 1
		tasks[i] = task
	}

	text, err := json.Marshal(tasks)
	checkError(err)

	err = os.WriteFile(repo.FilePath, text, 0644)
	checkError(err)

	return nil
}

// GetTasks returns all tasks from the repositories
func (repo *FileTaskRepository) GetTasks() ([]model.Todo, error) {
	text, err := os.ReadFile(repo.FilePath)
	checkError(err)
	if err != nil {
		return []model.Todo{}, err
	}

	var tasks []model.Todo
	err = json.Unmarshal(text, &tasks)
	checkError(err)

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (model.Todo, error) {
	tasks, err := repo.GetTasks()
	checkError(err)

	for _, task := range tasks {
		if task.ID == id {
			return task, nil
		}
	}

	return model.Todo{}, nil
}

// UpdateTask updates an existing task in the repositories
func (repo *FileTaskRepository) UpdateTask(task model.Todo) (model.Todo, error) {
	tasks, err := repo.GetTasks()
	checkError(err)

	for i, t := range tasks {
		if t.ID == task.ID {
			task.Status = "done"
			tasks[i] = task
			t.Status = "done"
			err = repo.SaveTasks(tasks)
			checkError(err)
			return task, nil
		}
	}
	var err1 error
	return task, err1
}

func (repo *FileTaskRepository) SaveTasks(tasks []model.Todo) error {
	text, err := os.ReadFile(repo.FilePath)
	checkError(err)

	var tasksText []model.Todo
	err = json.Unmarshal(text, &tasksText)

	var newTasks []model.Todo
	newTasks = append(tasksText, tasks...)

	txt, err := json.Marshal(newTasks)
	checkError(err)

	err = os.WriteFile(repo.FilePath, txt, 0644)
	checkError(err)

	return nil
}

func checkError(err error) {
	if err != nil {
		log.Println(err)
	}
}
