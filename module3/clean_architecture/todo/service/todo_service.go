package service

import (
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/model"
)

type TodoServiceInterface interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
}

type TaskRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) (model.Todo, error)
	UpdateTask(task model.Todo) (model.Todo, error)
	DeleteTask(id int) error
	UpdateForService(task model.Todo) (model.Todo, error)
	SaveTasksForService(tasks []model.Todo) error
}

type TodoService struct {
	Repository TaskRepository
}

func NewTodoService(r TaskRepository) *TodoService {
	return &TodoService{Repository: r}
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	tasks, err := s.Repository.GetTasks()
	if err != nil {
		return nil, err
	}

	var todos []model.Todo
	for _, task := range tasks {
		todos = append(todos, model.Todo{
			ID:          task.ID,
			Title:       task.Title,
			Status:      task.Status,
			Description: task.Description,
		})
	}

	return todos, nil
}

func (s *TodoService) CreateTodo(title, description string) error {
	_, err := s.Repository.CreateTask(model.Todo{
		Title:       title,
		Description: description,
		Status:      "open",
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) CompleteTodo(todo model.Todo) error {
	_, err := s.Repository.UpdateForService(model.Todo{
		ID:          todo.ID,
		Title:       todo.Title,
		Description: todo.Description,
		Status:      "done",
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *TodoService) RemoveTodo(todo model.Todo) error {
	err := s.Repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}
