package test

import (
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/model"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/repo"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/service"
	"os"
	"reflect"
	"testing"
)

var R = repo.NewFileTaskRepository("test_for_service.json")

func Test_todoService_CreateTodo(t *testing.T) {
	type args struct {
		title       string
		description string
	}
	tests := []struct {
		name    string
		fields  service.TodoService
		args    args
		wantErr bool
	}{
		{
			name:   "Test Create Todo",
			fields: service.TodoService{Repository: R},
			args: args{
				title:       "Test Todo",
				description: "Test Description",
			},
			wantErr: false,
		},
		{
			name:   "Test Create Todo1",
			fields: service.TodoService{Repository: R},
			args: args{
				title:       "Test Todo1",
				description: "Test Description1",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields.Repository,
			}
			if err := s.CreateTodo(tt.args.title, tt.args.description); (err != nil) != tt.wantErr {
				t.Errorf("CreateTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_CompleteTodo(t *testing.T) {
	tests := []struct {
		name    string
		fields  service.TodoRepository
		args    model.Todo
		wantErr bool
	}{
		{
			name:   "Test Complete Todo",
			fields: R,
			args: model.Todo{
				ID:          1,
				Title:       "Test Todo",
				Status:      "open",
				Description: "Test Description",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields,
			}
			if err := s.CompleteTodo(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("CompleteTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_todoService_ListTodos(t *testing.T) {

	tests := []struct {
		name    string
		fields  service.TodoRepository
		want    []model.Todo
		wantErr bool
	}{
		{
			name:   "Test List Todos",
			fields: R,
			want: []model.Todo{
				{
					ID:          1,
					Title:       "Test Todo",
					Status:      "done",
					Description: "Test Description",
				},
				{
					ID:          2,
					Title:       "Test Todo1",
					Status:      "open",
					Description: "Test Description1",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields,
			}
			got, err := s.ListTodos()
			if (err != nil) != tt.wantErr {
				t.Errorf("ListTodos() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListTodos() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_todoService_RemoveTodo(t *testing.T) {
	tests := []struct {
		name    string
		fields  service.TodoRepository
		args    model.Todo
		wantErr bool
	}{
		{
			name:   "Test Remove Todo",
			fields: R,
			args: model.Todo{
				ID:          2,
				Title:       "Test Todo1",
				Status:      "open",
				Description: "Test Description1",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &service.TodoService{
				Repository: tt.fields,
			}
			if err := s.RemoveTodo(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("RemoveTodo() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
	_ = os.Remove("test_for_service.json")
}
