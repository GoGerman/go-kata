package test

import (
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/model"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/repo"
	"os"
	"reflect"
	"testing"
)

func TestFileTaskRepository_CreateTask(t *testing.T) {
	tests := []struct {
		name    string
		fields  string
		args    model.Todo
		want    model.Todo
		wantErr bool
	}{
		{
			name:   "Create a new task1",
			fields: "test.json",
			args: model.Todo{
				ID:          1,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
			want: model.Todo{
				ID:          1,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
		},
		{
			name:   "Create a new task2",
			fields: "test.json",
			args: model.Todo{
				ID:          2,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
			want: model.Todo{
				ID:          2,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields,
			}
			got, err := repo.CreateTask(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_GetTask(t *testing.T) {
	var err error
	tests := []struct {
		name    string
		fields  string
		args    int
		want    model.Todo
		wantErr error
	}{
		{
			name:   "Get task1",
			fields: "test.json",
			args:   1,
			want: model.Todo{
				ID:          1,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
			wantErr: nil,
		},
		{
			name:    "Get task5",
			fields:  "test.json",
			args:    5,
			want:    model.Todo{},
			wantErr: err,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields,
			}
			got, err := repo.GetTask(tt.args)
			if err != nil {
				t.Errorf("GetTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_GetTasks(t *testing.T) {
	var err error
	tests := []struct {
		name    string
		fields  string
		want    []model.Todo
		wantErr error
	}{
		{
			name:   "Get all tasks",
			fields: "test.json",
			want: []model.Todo{
				{
					ID:          1,
					Title:       "Test",
					Description: "Test",
					Status:      "TODO",
				},
				{
					ID:          2,
					Title:       "Test",
					Description: "Test",
					Status:      "TODO",
				},
			},
			wantErr: nil,
		},
		{
			name:    "Get all tasks",
			fields:  "tes.json",
			want:    []model.Todo{},
			wantErr: err,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields,
			}
			got, err := repo.GetTasks()
			if tt.wantErr != nil {
				t.Errorf("GetTasks() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetTasks() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_SaveTasks(t *testing.T) {
	tests := []struct {
		name    string
		fields  string
		args    []model.Todo
		wantErr bool
	}{
		{
			name:   "Save tasks",
			fields: "test.json",
			args: []model.Todo{
				{
					ID:          3,
					Title:       "Test",
					Description: "Test",
					Status:      "TODO",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields,
			}
			if err := repo.SaveTasks(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("SaveTasks() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFileTaskRepository_UpdateTask(t *testing.T) {
	var err error
	tests := []struct {
		name    string
		fields  string
		args    model.Todo
		want    model.Todo
		wantErr error
	}{
		{
			name:   "Update task1",
			fields: "test.json",
			args: model.Todo{
				ID:          1,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
			want: model.Todo{
				ID:          1,
				Title:       "Test",
				Description: "Test",
				Status:      "done",
			},
			wantErr: nil,
		},
		{
			name:   "Update task2",
			fields: "test.json",
			args: model.Todo{
				ID:          6,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
			want: model.Todo{
				ID:          6,
				Title:       "Test",
				Description: "Test",
				Status:      "TODO",
			},
			wantErr: err,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields,
			}
			got, err := repo.UpdateTask(tt.args)
			if tt.wantErr != nil {
				t.Errorf("UpdateTask() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateTask() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFileTaskRepository_DeleteTask(t *testing.T) {
	var err error
	tests := []struct {
		name   string
		fields string
		args   int
		err    error
	}{
		{
			name:   "Delete a task",
			fields: "test.json",
			args:   1,
			err:    nil,
		},
		{
			name:   "Delete a task",
			fields: "test.json",
			args:   10,
			err:    err,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &repo.FileTaskRepository{
				FilePath: tt.fields,
			}
			if err := repo.DeleteTask(tt.args); tt.err != nil {
				t.Errorf("DeleteTask() error = %v, wantErr %v", err, tt.err)
			}
		})
	}
	_ = os.Remove("test.json")
}
