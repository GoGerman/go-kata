package main

import (
	"fmt"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/model"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/repo"
	"gitlab.com/GoGerman/go-kata/module3/clean_architecture/todo/service"
	"os"
)

func main() {
	file, err := os.OpenFile("./test.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	rep := test1(file)

	todo := service.NewTodoService(rep)
	todos, err := todo.ListTodos()
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(todos)
}

func test1(file *os.File) *repo.FileTaskRepository {
	rep := repo.NewFileTaskRepository(file.Name())

	task := []model.Todo{
		{
			Title:       "Test",
			Description: "Test",
			Status:      "open",
			ID:          0,
		},
		{
			Title:       "Test",
			Description: "Test",
			Status:      "open",
			ID:          1,
		},
		{
			Title:       "Test",
			Description: "Test",
			Status:      "open",
			ID:          2,
		},
	}

	for _, i := range task {
		_, _ = rep.CreateTask(i)
	}

	return rep
}
