package main

import (
	"fmt"
	"gitlab/go-kata/module3/clean_architecture/repo/repository"
	"io"
	"os"
)

func main() {
	tests := []repository.User{
		{ID: 0, Name: "Evg"},
		{ID: 1, Name: "Dima"},
		{ID: 2, Name: "Mitya"},
	}
	fl, err := os.OpenFile("./test11.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		panic(err)
	}
	reader := io.ReadWriter(fl)
	file, err := repository.NewUserRepository(reader)
	if err != nil {
		panic(err)
	}
	for _, i := range tests {
		file.Save(i)
	}
	res, _ := file.FindAll()
	res1, _ := file.Find(1)
	fmt.Println(res)
	fmt.Println(res1)
}
