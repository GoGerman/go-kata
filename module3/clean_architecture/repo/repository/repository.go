package repository

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File *os.File
	User []User
}

func NewUserRepository(fileRW io.ReadWriter) (*UserRepository, error) {
	file, ok := fileRW.(*os.File)
	if !ok {
		return nil, fmt.Errorf("fileRW is not *os.File")
	}
	return &UserRepository{
		File: file,
	}, nil
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (r *UserRepository) Save(record interface{}) error {
	r.User = append(r.User, record.(User))
	if _, err := r.File.Seek(0, 0); err != nil {
		return err
	}
	text, _ := json.Marshal(r.User)
	_, err := r.File.Write(text)
	if err != nil {
		return err
	}
	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	var err error
	var res []User
	for i := range r.User {
		if r.User[i].ID == id {
			res = append(res, r.User[i])
			return res, nil
		}
	}
	return []User{}, err
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	var err error
	var res []interface{}
	for i := range r.User {
		res = append(res, r.User[i])
	}
	return res, err
}
