package repository

import (
	"fmt"
	"io"
	"os"
	"reflect"
	"testing"
)

func NewUserRepositoryForTest() io.ReadWriter {
	fl, err := os.OpenFile("./test.json", os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		panic(err)
	}
	reader := io.ReadWriter(fl)
	return reader
}

var Reader = NewUserRepositoryForTest()
var Repos, _ = NewUserRepository(Reader)

func TestUserRepository_Save(t *testing.T) {
	tests := []struct {
		name string
		args interface{}
		want interface{}
	}{
		{
			name: "test 1",
			args: User{ID: 0, Name: "Evg"},
			want: `[{"id":0,"name":"Evg"}]` + "\n",
		},
		{
			name: "test 2",
			args: User{ID: 1, Name: "Dima"},
			want: `[{"id":0,"name":"Evg"},{"id":1,"name":"Dima"}]` + "\n",
		},
		{
			name: "test 3",
			args: User{ID: 2, Name: "Mitya"},
			want: `[{"id":0,"name":"Evg"},{"id":1,"name":"Dima"},{ID: 2, Name: "Mitya"}]` + "\n",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Repos.Save(tt.args); err != nil {
				t.Errorf("Save() error = %v, want %v", err, tt.want)
			}
			got, _ := os.ReadFile(Repos.File.Name())
			if reflect.DeepEqual(string(got), tt.want) {
				t.Errorf("Save() got = %v, want %v", string(got), tt.want)
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	var err = fmt.Errorf("user not found")
	tests := []struct {
		name    string
		fields  interface{}
		args    int
		want    []User
		wantErr error
	}{
		{
			name:    "test 1",
			fields:  Repos,
			want:    []User{{ID: 2, Name: "Mitya"}},
			args:    2,
			wantErr: nil,
		},
		{
			name:    "test 2",
			fields:  Repos,
			want:    []User{},
			args:    5,
			wantErr: err,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Repos.Find(tt.args)
			if err != nil && err != tt.wantErr {
				t.Errorf("want %q, got %q", err, tt.wantErr)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Find() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	tests := []struct {
		name   string
		fields *UserRepository
		want   []User
	}{
		{
			name:   "test 1",
			fields: Repos,
			want:   []User{{ID: 0, Name: "Evg"}, {ID: 1, Name: "Dima"}, {ID: 2, Name: "Mitya"}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Repos.FindAll()
			if err != nil {
				fmt.Println(err)
			}
			if len(got) != len(tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
	_ = os.Remove(Repos.File.Name())
}
