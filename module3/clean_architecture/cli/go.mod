module gitlab/go-kata/module3/clean_architecture/cli

go 1.20

require gitlab.com/GoGerman/go-kata v0.0.0-20230410111813-15ccc345505d

require (
	github.com/go-chi/chi/v5 v5.0.8 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v1.14.16 // indirect
	github.com/prometheus/client_golang v1.14.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
