package main

import (
	"bufio"
	"fmt"
	"gitlab/go-kata/module3/clean_architecture/cli/client"
	"gitlab/go-kata/module3/clean_architecture/cli/repository"
	"gitlab/go-kata/module3/clean_architecture/cli/service"
	"log"
	"os"
)

func main() {
	file, err := os.OpenFile("./test.json", os.O_CREATE|os.O_RDWR, 0644)
	checkErr(err)

	repo := repository.NewFileTaskRepository(file.Name())
	serv := service.NewTodoService(repo)

	in := bufio.NewReader(os.Stdin)
	text := bufio.NewScanner(in)

	client.Timer()
	client.ClearAll()

	for {
	A:
		num := client.ChooseNumber()
		switch num {
		case 1:
			client.Case1(serv)
			goto A
		case 2:
			client.Case2(serv, text)
			goto A
		case 3:
			client.Case3(serv)
			goto A
		case 4:
			client.Case4(serv, text)
			goto A
		case 5:
			client.Case5(serv)
			goto A
		case 6:
			fmt.Println("\nПрограмма завершена\nДо свидания")
			os.Exit(1)
		}
	}

}

func checkErr(err error) {
	if err != nil {
		log.Println(err)
	}
}
