package client

import (
	"bufio"
	"fmt"
	"gitlab/go-kata/module3/clean_architecture/cli/model"
	"gitlab/go-kata/module3/clean_architecture/cli/service"
	"log"
	"time"
)

func Timer() {
	fmt.Println("Запуск...")
	time.Sleep(1 * time.Second)
	fmt.Println(".")
	time.Sleep(1 * time.Second)
	fmt.Println("..")
	time.Sleep(1 * time.Second)
	fmt.Println("...")
	time.Sleep(1 * time.Second)
}

func checkErr(err error) {
	if err != nil {
		log.Println(err)
	}
}

func ClearAll() {
	fmt.Print("\x1b[2J")
}

func ChooseNumber() int {
	var num int
	fmt.Println("--------Меню--------\n" +
		"1. Показать список задач.\n" +
		"2. Добавить задачу.\n" +
		"3. Удалить задачу.\n" +
		"4. Редактировать задачу по ID.\n" +
		"5. Завершить задачу.\n" +
		"6. Exit.")
	fmt.Println("\nВведите число:")
	_, err := fmt.Scanf("%d", &num)
	if err != nil {
		fmt.Println(err)
		//ClearAll()
		ChooseNumber()
	}
	return num
}

func Case1(serv *service.TodoService) {
	list, err := serv.ListTodos()
	checkErr(err)
	ClearAll()
	if len(list) < 1 {
		fmt.Println("Список пуст")
	}
	fmt.Println(list)
	fmt.Printf("Для перехода в основное меню нажми \"Enter\"\n")
	_, err = fmt.Scanln()
	checkErr(err)
	ClearAll()
}

func Case2(serv *service.TodoService, text *bufio.Scanner) {
	var title, description string
	fmt.Println("Напишите заголовок:")
	text.Scan()
	title = text.Text()
	fmt.Println("Напишите описание:")
	text.Scan()
	description = text.Text()
	err := serv.CreateTodo(title, description)
	checkErr(err)
	fmt.Println()
	fmt.Println("Задача создана")
	fmt.Printf("Для перехода в основное меню нажми \"Enter\"\n")
	_, err = fmt.Scanln()
	checkErr(err)
	ClearAll()
}

func Case3(serv *service.TodoService) {
	var num int
A:
	fmt.Println("Введите ID задачи, которую хотите удалить:")
	_, err := fmt.Scanf("%d", &num)
	if err != nil {
		fmt.Println("\nОшибка\nПожалуйста введите число")
		goto A
	}
	todo := model.Todo{
		ID: num,
	}
	err = serv.RemoveTodo(todo)
	checkErr(err)
	fmt.Println("\nЗадача удалена")
	fmt.Printf("\nДля перехода в основное меню нажми \"Enter\"\n")
	_, err = fmt.Scanln()
	checkErr(err)
	ClearAll()
}

func Case4(serv *service.TodoService, text *bufio.Scanner) {
	var num int
	var title, description string
A:
	fmt.Println("Введите ID задачи, которую хотите изменить:")
	_, err := fmt.Scanf("%d", &num)
	if err != nil {
		fmt.Println("\nОшибка\nПожалуйста введите число")
		goto A
	}

	fmt.Println("Выберите цифру того, что вы хотите изменить:\n1. Заголовок\n2. Описание\n3. Заголовок и описание")
	var newNum int
	_, err = fmt.Scanf("%d", &newNum)

	var todo model.Todo
	switch newNum {
	case 1:
		fmt.Println("\nВведите новый заголовок:")
		text.Scan()
		title = text.Text()

		todo = model.Todo{
			ID:    num,
			Title: title,
		}
	case 2:
		fmt.Println("Введите новое описание:")
		text.Scan()
		description = text.Text()

		todo = model.Todo{
			ID:          num,
			Description: description,
		}
	case 3:
		fmt.Println("\nВведите новый заголовок:")
		text.Scan()
		title = text.Text()

		fmt.Println("Введите новое описание:")
		text.Scan()
		description = text.Text()

		todo = model.Todo{
			ID:          num,
			Description: description,
			Title:       title,
		}
	default:
	}

	err = serv.UpdateTitleDescription(todo)
	checkErr(err)
	fmt.Println("\nЗадача выполнена")
	fmt.Printf("\nДля перехода в основное меню нажми \"Enter\"\n")
	_, err = fmt.Scanln()
	checkErr(err)
	ClearAll()
}

func Case5(serv *service.TodoService) {
	var num int
A:
	fmt.Println("Введите ID задачи, которую хотите завершить:")
	_, err := fmt.Scanf("%d", &num)
	if err != nil {
		fmt.Println("\nОшибка\nПожалуйста введите число")
		goto A
	}
	todo := model.Todo{
		ID: num,
	}
	err = serv.CompleteTodo(todo)
	checkErr(err)
	fmt.Println("\nЗадача выполнена")
	fmt.Printf("\nДля перехода в основное меню нажми \"Enter\"\n")
	_, err = fmt.Scanln()
	checkErr(err)
	ClearAll()
}
