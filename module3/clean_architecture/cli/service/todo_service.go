package service

import (
	"gitlab/go-kata/module3/clean_architecture/cli/model"
)

type TodoServiceInterface interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
	UpdateToDo(todo model.Todo) error
}

type TaskRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) (model.Todo, error)
	UpdateTask(task model.Todo) (model.Todo, error)
	DeleteTask(id int) error
	UpdateForService(task model.Todo) (model.Todo, error)
	SaveTasksForService(tasks []model.Todo) error
	SaveTasks(tasks []model.Todo) error
}

type TodoService struct {
	repository TaskRepository
}

func NewTodoService(r TaskRepository) *TodoService {
	return &TodoService{repository: r}
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	tasks, err := s.repository.GetTasks()
	if err != nil {
		return nil, err
	}

	var todos []model.Todo
	for _, task := range tasks {
		todos = append(todos, model.Todo{
			ID:          task.ID,
			Title:       task.Title,
			Status:      task.Status,
			Description: task.Description,
		})
	}

	return todos, nil
}

func (s *TodoService) CreateTodo(title, description string) error {
	_, err := s.repository.CreateTask(model.Todo{
		Title:       title,
		Description: description,
		Status:      "open",
	})
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) CompleteTodo(todo model.Todo) error {
	_, err := s.repository.UpdateForService(model.Todo{
		ID:          todo.ID,
		Title:       todo.Title,
		Description: todo.Description,
		Status:      "done",
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *TodoService) RemoveTodo(todo model.Todo) error {
	err := s.repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}

func (s *TodoService) UpdateToDo(todo model.Todo) error {
	tasks, err := s.ListTodos()
	for n, i := range tasks {
		if i.ID == todo.ID {
			tasks[n] = model.Todo{
				ID:          todo.ID,
				Title:       todo.Title,
				Description: todo.Description,
				Status:      "open",
			}
		}
	}
	if err != nil {
		return err
	}

	return nil
}

func (s *TodoService) UpdateTitleDescription(todo model.Todo) error {
	tasks, err := s.ListTodos()
	if err != nil {
		return err
	}
	for n, i := range tasks {
		if i.ID == todo.ID {
			tasks[n] = model.Todo{
				ID:          todo.ID,
				Title:       todo.Title,
				Description: todo.Description,
				Status:      "open",
			}
		}
	}
	var repo []model.Todo
	for _, i := range tasks {
		rep := model.Todo{
			ID:          i.ID,
			Title:       i.Title,
			Description: i.Description,
			Status:      "open",
		}
		repo = append(repo, rep)
	}
	err = s.repository.SaveTasksForService(repo)
	if err != nil {
		return err
	}

	return nil
}
