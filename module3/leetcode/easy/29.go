package easy

func xorOperation(n int, start int) int {
	ret := start
	for i := 1; i < n; i++ {
		num := start + 2*i
		ret ^= num
	}
	return ret
}
