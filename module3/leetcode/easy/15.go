package easy

type ParkingSystem struct {
	small, medium, big int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{big: big, medium: medium, small: small}
}

func (this *ParkingSystem) AddCar(carType int) bool {
	switch carType {
	case 1:
		if this.big != 0 {
			this.big -= 1
			return true
		}
	case 2:
		if this.medium != 0 {
			this.medium -= 1
			return true
		}
	case 3:
		if this.small != 0 {
			this.small -= 1
			return true
		}
	}
	return false
}
