package easy

func smallerNumbersThanCurrent(nums []int) []int {
	num := []int{}
	count := 0
	for i := 0; i < len(nums); i++ {
		count = 0
		for j := 0; j < len(nums); j++ {
			if nums[i] > nums[j] {
				count++
			}
		}
		num = append(num, count)
	}
	return num
}
