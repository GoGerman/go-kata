package easy

func runningSum(nums []int) []int {
	var (
		n   int
		num []int
	)

	for i := 0; i < len(nums); i++ {
		n += nums[i]
		num = append(num, n)
	}
	return num
}
