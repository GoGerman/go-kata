package easy

func tribonacci(n int) int {
	switch n {
	case 0:
		return 0
	case 1:
		return 1
	case 2:
		return 1
	case 3:
		return 2
	default:
		zero, one, two, count := 0, 1, 1, 2
		for count != n {
			zero, one, two = one, two, zero+one+two
			count++
		}
		return two
	}
}
