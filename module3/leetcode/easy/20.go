package easy

import "sort"

func kidsWithCandies(candies []int, extraCandies int) []bool {
	var bl []bool
	var candy []int
	candy = append(candy, candies...)
	sort.Ints(candy)
	for i := 0; i < len(candies); i++ {
		if candies[i]+extraCandies >= candy[len(candy)-1] {
			bl = append(bl, true)
		} else {
			bl = append(bl, false)
		}
	}

	return bl
}
