package easy

func shuffle(nums []int, n int) []int {
	var num []int
	for i := 0; i < len(nums)/2; i++ {
		num = append(num, nums[i], nums[i+n])
	}
	return num
}
