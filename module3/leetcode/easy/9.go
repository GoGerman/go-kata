package easy

func finalValueAfterOperations(operations []string) int {
	num := 0
	for _, n := range operations {
		if string(n[1]) == "+" {
			num++
		} else {
			num--
		}
	}
	return num
}
