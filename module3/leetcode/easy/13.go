package easy

import "strings"

func numJewelsInStones(jewels string, stones string) int {
	//return strings.Count(stones, jewels)
	count := 0
	for _, v := range jewels {
		count += strings.Count(stones, string(v))
	}
	return count
}
