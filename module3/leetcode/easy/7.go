package easy

import "strings"

func defangIPaddr(address string) string {
	return strings.Join(strings.Split(address, "."), "[.]")
}
