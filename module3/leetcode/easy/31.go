package easy

import "sort"

func sortPeople(names []string, heights []int) []string {
	hash := make(map[int]string)
	ans := []string{}

	for i, v := range heights {
		hash[v] = names[i]
	}
	sort.Sort(sort.Reverse(sort.IntSlice(heights)))

	for _, v := range heights {
		ans = append(ans, hash[v])
	}

	return ans
}
