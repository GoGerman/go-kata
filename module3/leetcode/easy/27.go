package easy

func balancedStringSplit(s string) int {
	a, b, c := 0, 0, 0
	for _, i := range s {
		if i == 'R' {
			a++
		} else {
			b++
		}
		if a == b {
			a, b = 0, 0
			c++
		}
	}
	return c
}
