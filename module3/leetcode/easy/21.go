package easy

func subtractProductAndSum(n int) int {
	product, summ := 1, 0
	for n > 0 {
		summ += n % 10
		product *= n % 10
		n /= 10
	}
	return product - summ
}
