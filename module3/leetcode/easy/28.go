package easy

func countDigits(num int) int {
	count, x := 0, num
	for x > 0 {
		d := x % 10
		if num%d == 0 {
			count++
		}
		x /= 10
	}
	return count
}
