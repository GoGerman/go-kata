package easy

func createTargetArray(nums []int, index []int) []int {
	res := make([]int, len(nums))
	for idx, i := range index {
		for j := len(res) - 1; j > i; j-- {
			res[j] = res[j-1]
		}
		res[i] = nums[idx]
	}
	return res
}
