package easy

import "strings"

func mostWordsFound(sentences []string) int {
	count := 0
	for _, sent := range sentences {
		count1 := len(strings.Split(sent, " "))
		if count1 > count {
			count = count1
		}
	}
	return count
}
