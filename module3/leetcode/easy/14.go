package easy

func maximumWealth(accounts [][]int) int {
	max, count := 0, 0

	for i := 0; i < len(accounts); i++ {
		for j := 0; j < len(accounts[i]); j++ {
			count += accounts[i][j]
		}
		if count > max {
			max = count
		}
		count = 0
	}
	return max
}
