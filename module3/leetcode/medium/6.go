package medium

func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}

	maxIdx := findMaxIdx(nums)

	return &TreeNode{Val: nums[maxIdx],
		Left:  constructMaximumBinaryTree(nums[:maxIdx]),
		Right: constructMaximumBinaryTree(nums[maxIdx+1:])}
}

// findMaxIdx takes a slice of ints and returns the index of the largest int
func findMaxIdx(nums []int) int {
	max := 0
	for i := 1; i < len(nums); i++ {
		if nums[i] > nums[max] {
			max = i
		}
	}

	return max
}
