package medium

type ListNode struct {
	Val  int
	Next *ListNode
}

func mergeNodes(head *ListNode) *ListNode {
	newList := &ListNode{}
	cur, sum := newList, 0
	for head != nil {
		if head.Val == 0 && sum != 0 {
			cur.Next = &ListNode{sum, nil}
			cur = cur.Next
			sum = 0
		}
		sum += head.Val
		head = head.Next
	}
	return newList.Next
}
