package medium

import "sort"

func numTilePossibilities(tiles string) int {
	count := 0
	arr := []byte(tiles)
	bites := make([]bool, len(tiles))
	sort.Slice(arr, func(i, j int) bool {
		return arr[i] < arr[j]
	})
	var rec func()
	rec = func() {
		for i := 0; i < len(arr); i++ {
			if bites[i] {
				continue
			}
			bites[i] = true
			count++
			rec()
			bites[i] = false
			for ; i+1 < len(arr) && arr[i] == arr[i+1]; i++ {
			}
		}
	}
	rec()
	return count
}
