package medium

func xorQueries(arr []int, queries [][]int) []int {
	var n, m = len(arr), len(queries)

	var val, ans []int
	var i int

	val = append(val, arr[0])

	for i = 1; i < n; i++ {
		val = append(val, val[i-1]^arr[i])
	}
	for i = 0; i < m; i++ {
		if queries[i][0] == 0 {
			ans = append(ans, val[queries[i][1]])

		} else {
			ans = append(ans, val[queries[i][0]-1]^val[queries[i][1]])

		}
	}
	return ans
}
