package medium

func averageOfSubtree(root *TreeNode) int {
	count := 0
	postOrderTrav(root, &count)
	return count
}

func postOrderTrav(root *TreeNode, count *int) []int {
	if root == nil {
		return []int{0, 0}
	}
	l := postOrderTrav(root.Left, count)
	r := postOrderTrav(root.Right, count)

	if root.Val == (l[0]+r[0]+root.Val)/(l[1]+r[1]+1) {
		*count++
	}
	return []int{l[0] + r[0] + root.Val, l[1] + r[1] + 1}
}
