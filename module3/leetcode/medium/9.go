package medium

import "sort"

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	m := len(l)
	ans := make([]bool, m)
	var helper func(arr []int) bool
	helper = func(arr []int) bool {
		diff := arr[0] - arr[1]
		for i := 2; i < len(arr); i++ {
			if arr[i-1]-arr[i] != diff {
				return false
			}
		}
		return true
	}
	for i := 0; i < m; i++ {
		tmp := make([]int, r[i]+1-l[i])
		copy(tmp, nums[l[i]:r[i]+1])
		sort.Ints(tmp)
		if helper(tmp) {
			ans[i] = true
		} else {
			ans[i] = false
		}
	}
	return ans
}
