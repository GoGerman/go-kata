package medium

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func deepestLeavesSum(root *TreeNode) int {
	var sum int
	thr := []TreeNode{*root}
	for len(thr) != 0 {
		sum = 0
		n := len(thr)
		for i := 0; i < n; i++ {
			node := thr[0]
			thr = thr[1:]
			sum += node.Val
			if node.Left != nil {
				thr = append(thr, *node.Left)
			}
			if node.Right != nil {
				thr = append(thr, *node.Right)
			}
		}
	}
	return sum
}
