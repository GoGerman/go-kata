package medium

func max(a, b int) int {
	if a >= b {
		return a
	}
	return b
}

func maxSum(grid [][]int) int {
	result := 0
	for i := 2; i < len(grid); i++ {
		for j := 2; j < len(grid[0]); j++ {
			sum := grid[i-2][j-2] + grid[i-2][j-1] + grid[i-2][j] + grid[i-1][j-1] + grid[i][j-2] + grid[i][j-1] + grid[i][j]
			result = max(result, sum)
		}
	}
	return result
}
