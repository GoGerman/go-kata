package medium

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if helperFunc(root, target) {
		return nil
	}
	return root
}

func helperFunc(node *TreeNode, target int) bool {
	if node == nil {
		return false
	}
	if helperFunc(node.Left, target) {
		node.Left = nil
	}
	if helperFunc(node.Right, target) {
		node.Right = nil
	}
	if node.Left == nil && node.Right == nil && node.Val == target {
		return true
	}
	return false
}
