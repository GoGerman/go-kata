package main

import (
	"fmt"
	"os"
	"strconv"
	"sync"
)

func main() {

	var wg sync.WaitGroup

	wg.Add(2)
	go func() {
		_ = os.Mkdir("/Users/pusichkagerman/go/src/GoKata/go-kata/module3/leetcode/easy", 0777)
		for i := 1; i < 32; i++ {
			name := strconv.Itoa(i)
			_, err := os.Create("/Users/pusichkagerman/go/src/GoKata/go-kata/module3/leetcode/easy/" + name + ".go")
			if err != nil {
				fmt.Println("Error")
			}
		}
		wg.Done()
	}()
	go func() {
		_ = os.Mkdir("/Users/pusichkagerman/go/src/GoKata/go-kata/module3/leetcode/medium", 0777)
		for i := 1; i < 21; i++ {
			name := strconv.Itoa(i)
			_, err := os.Create("/Users/pusichkagerman/go/src/GoKata/go-kata/module3/leetcode/medium/" + name + ".go")
			if err != nil {
				fmt.Println("Error")
			}
		}
		wg.Done()
	}()
	wg.Wait()
}
