package main

import "fmt"

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

type RealAirConditioner struct{}

type AirConditionerAdapter struct {
	airConditioner AirConditioner
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a *AirConditionerAdapter) SetTemperature(temp int) {
	a.airConditioner.SetTemperature(temp)
}

func (p *AirConditionerProxy) TurnOn() {
	if p.authenticated {
		p.adapter.TurnOn()
	} else {
		fmt.Println("Access denied. User not authenticated.")
	}
}

func (p *AirConditionerProxy) TurnOff() {
	if p.authenticated {
		p.adapter.TurnOff()
	} else {
		fmt.Println("Access denied. User not authenticated.")
	}
}

func (p *AirConditionerProxy) SetTemperature(temp int) {
	if p.authenticated {
		p.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied. User not authenticated.")
	}
}

func (a *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
}

func (a *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
}

func (a *RealAirConditioner) SetTemperature(temp int) {
	fmt.Printf("Setting the temperature to %d\n", temp)
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	realAC := &RealAirConditioner{}
	adapter := &AirConditionerAdapter{airConditioner: realAC}
	return &AirConditionerProxy{adapter: adapter, authenticated: authenticated}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
