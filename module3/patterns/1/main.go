package main

import "fmt"

type PricingStrategy interface {
	Calculate(order Order) float64
}

type Order struct {
	name     string
	price    float64
	quantity float64
}

type RegularPricing struct{}

type SalePricing struct {
	discount float64
}

func (r RegularPricing) Calculate(order Order) float64 {
	return order.price * order.quantity
}

func (s SalePricing) Calculate(order Order) float64 {
	return order.price * (1 - s.discount/100) * order.quantity
}

func newOrder(name string, price, quantity float64) *Order {
	return &Order{name: name, price: price, quantity: quantity}
}

func checkOut(order *Order, ps PricingStrategy) float64 {
	return ps.Calculate(*order)
}

func main() {
	order := newOrder("televizor", 10000, 23)
	sliceInterface := []PricingStrategy{
		RegularPricing{},
		SalePricing{10},
		SalePricing{20},
		SalePricing{30},
	}
	for i := range sliceInterface {
		price := checkOut(order, sliceInterface[i])
		if i == 0 {
			fmt.Printf("Total cost with discount 0 strategy: %v\n", price)
		} else {
			fmt.Printf("Total cost with %#v strategy: %v\n", sliceInterface[i], price)
		}
	}
}
