package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
	GetApi() string
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	sp, _ := strconv.Atoi(location)
	return sp
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	sp, _ := strconv.Atoi(location)
	return sp
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	sp, _ := strconv.Atoi(location)
	return sp
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (o *OpenWeatherAPI) GetApi() string {
	return o.apiKey
}

func (w *WeatherFacade) GetApiKey() string {
	return w.weatherAPI.GetApi()
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	resp, err := http.Get(fmt.Sprintf("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s",
		location, w.GetApiKey()))
	if err != nil {
		log.Println("Не могу получить данные")
	}
	defer resp.Body.Close()

	var weatherData struct {
		Main struct {
			Temp     float64 `json:"temp"`
			Humidity int     `json:"humidity"`
		} `json:"main"`
		Wind struct {
			Speed float64 `json:"speed"`
		} `json:"wind"`
	}

	if err := json.NewDecoder(resp.Body).Decode(&weatherData); err != nil {
		log.Println("Не могу декодировать данные")
	}

	temperature := w.weatherAPI.GetTemperature(strconv.Itoa(int(weatherData.Main.Temp - 273.15)))
	humidity := w.weatherAPI.GetHumidity(strconv.Itoa(weatherData.Main.Humidity))
	windSpeed := w.weatherAPI.GetWindSpeed(strconv.Itoa(int(weatherData.Main.Temp)))

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{
			apiKey: apiKey,
		},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("78cfb04016855233daaf20a3817aefa3")
	cities := []string{"Москва", "Saint Petersburg", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}
}
