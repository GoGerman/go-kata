package main

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"
)

func Test_mergeSort(t *testing.T) {
	data := randomData(100, 1000)
	var sorted []int
	sorted = append(sorted, data...)
	sort.Ints(sorted)
	type args struct {
		items []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Test QuickSort",
			args: args{
				items: []int{1, 4, 56, 435, 65, 87, 76, 7889, 776, 543, 5},
			},
			want: []int{1, 4, 5, 56, 65, 76, 87, 435, 543, 776, 7889},
		},
		{
			name: "Test QuickSort 1000 numbers",
			args: args{
				items: data,
			},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mergeSort(tt.args.items); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("mergeSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkMergeSort(b *testing.B) {
	dataSet := randomData(10000, 50000)
	for i := 0; i < b.N; i++ {
		mergeSort(dataSet)
	}
}

func randomData(n, max int) []int {
	var data []int
	if data != nil {
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}
	}
	return data
}
