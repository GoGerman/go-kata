package main

func mergeSort(items []int) []int {
	if len(items) < 2 {
		return items
	}

	first := mergeSort(items[:len(items)/2])
	second := mergeSort(items[len(items)/2:])

	return merge(first, second)
}

func merge(first, second []int) []int {
	var result []int

	i := 0
	j := 0

	for i < len(first) && j < len(second) {
		if first[i] < second[j] {
			result = append(result, first[i])
			i++
		} else {
			result = append(result, second[j])
			j++
		}
	}

	for i < len(first) {
		result = append(result, first[i])
		i++
	}

	for j < len(second) {
		result = append(result, second[j])
		j++
	}

	return result
}
