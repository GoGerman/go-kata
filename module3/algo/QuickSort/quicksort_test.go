package main

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"
)

func TestQuickSort(t *testing.T) {
	data := randomData(100, 1000)
	var sorted []int
	sorted = append(sorted, data...)
	sort.Ints(sorted)
	type args struct {
		arr []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Test QuickSort",
			args: args{
				arr: []int{1, 4, 56, 435, 65, 87, 76, 7889, 776, 543, 5},
			},
			want: []int{1, 4, 5, 56, 65, 76, 87, 435, 543, 776, 7889},
		},
		{
			name: "Test QuickSort 1000 numbers",
			args: args{
				arr: data,
			},
			want: sorted,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := QuickSort(tt.args.arr); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("QuickSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func randomData(n, max int) []int {
	var data []int
	if data != nil {
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}
	}
	return data
}
