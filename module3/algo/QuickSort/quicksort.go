package main

import "math/rand"

func QuickSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))]

	lowPart := make([]int, 0, len(arr))
	middlePart := make([]int, 0, len(arr))
	highPart := make([]int, 0, len(arr))

	for _, v := range arr {
		switch {
		case v < median:
			lowPart = append(lowPart, v)
		case v > median:
			highPart = append(highPart, v)
		default:
			middlePart = append(middlePart, v)
		}
	}

	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}
