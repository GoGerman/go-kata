package main

import (
	"math/rand"
	"testing"
)

var sls = func() []int {
	var sl []int
	for len(sl) < 100 {
		num := rand.Intn(100)
		sl = append(sl, num)
	}
	return sl
}

var sl = sls()

var min, max = 0, 10000000000

func BenchmarkRadixSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		RadixSort(sl)
	}
}

func BenchmarkCountingSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		CountingSort(sl, min, max)
	}
}

func BenchmarkQuickSort(b *testing.B) {
	for i := 0; i < b.N; i++ {
		QuickSort(sl)
	}
}
