package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"strings"
	"sync"
)

func main() {
	var sl []int

	for len(sl) < 1000000 {
		num := rand.Intn(10000000)
		sl = append(sl, num)
	}

	//	Buble(sl)
	QuickSort(sl)

	fmt.Println(sl)
}

func Buble(sl []int) {
	for i := range sl {
		for j := 0; j < len(sl)-i-1; j++ {
			if sl[j] > sl[j+1] {
				sl[j], sl[j+1] = sl[j+1], sl[j]
			}
		}
	}
}

func QuickSort(arr []int) []int {
	if len(arr) <= 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))]

	lowPart := make([]int, 0, len(arr))
	middlePart := make([]int, 0, len(arr))
	highPart := make([]int, 0, len(arr))

	for _, v := range arr {
		switch {
		case v < median:
			lowPart = append(lowPart, v)
		case v > median:
			highPart = append(highPart, v)
		default:
			middlePart = append(middlePart, v)
		}
	}

	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)

	return lowPart
}

func CountingSort(list []int, min, max int) {
	defer func() {
		if x := recover(); x != nil {
			if _, ok := x.(runtime.Error); ok &&
				strings.HasSuffix(x.(error).Error(), "index out of range") {
				fmt.Printf("data value out of range (%d..%d)\n", min, max)
				return
			}
		}
	}()

	count := make([]int, max-min+1)
	for _, x := range list {
		count[x-min]++
	}

	z := 0
	for i, c := range count {
		for ; c > 0; c-- {
			list[z] = i + min
			z++
		}
	}
}

func findLargestNum(array []int) int {
	largestNum := 0

	for i := 0; i < len(array); i++ {
		if array[i] > largestNum {
			largestNum = array[i]
		}
	}
	return largestNum
}

func RadixSort(array []int) {

	largestNum := findLargestNum(array)
	size := len(array)
	significantDigit := 1
	semiSorted := make([]int, size, size)

	for largestNum/significantDigit > 0 {

		bucket := [10]int{0}

		wg := sync.WaitGroup{}
		wg.Add(2)

		go func() {
			for i := 0; i < size; i++ {
				bucket[(array[i]/significantDigit)%10]++
			}
			wg.Done()
		}()

		go func() {
			for i := 1; i < 10; i++ {
				bucket[i] += bucket[i-1]
			}
			wg.Done()
		}()

		wg.Wait()

		for i := size - 1; i >= 0; i-- {
			bucket[(array[i]/significantDigit)%10]--
			semiSorted[bucket[(array[i]/significantDigit)%10]] = array[i]
		}
		for i := 0; i < size; i++ {
			array[i] = semiSorted[i]
		}

		significantDigit *= 10
	}

	//return array
}
