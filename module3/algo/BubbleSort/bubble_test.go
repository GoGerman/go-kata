package main

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"
)

func TestBubbleSort(t *testing.T) {
	type args struct {
		data []int
	}
	var data, dataSort []int
	for i := 0; i < 100; i++ {
		rand.Seed(time.Now().UnixNano())
		n := rand.Intn(1000)
		data = append(data, n)
	}
	dataSort = data
	sort.Ints(dataSort)
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort",
			args: args{
				data: data,
			},
			want: dataSort,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BubbleSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("BubbleSort() = %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkBubbleSort(b *testing.B) {
	var data []int
	for i := 0; i < 100; i++ {
		rand.Seed(time.Now().UnixNano())
		n := rand.Intn(1000)
		data = append(data, n)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}
