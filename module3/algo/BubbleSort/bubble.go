package main

func BubbleSort(data []int) []int {
	swap := true
	i := 1
	for swap {
		swap = false
		for j := 0; j < len(data)-i; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swap = true
			}
		}
		i++
	}
	return data
}
