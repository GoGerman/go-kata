package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup

	wg.Add(3)
	go func() {
		//sl1 := make([]int, 100000000)
		//for i := 0; i < 100000000; i++ {
		//	num := rand.Int()
		//	sl1[i] = num
		//}
		//k := findMax(sl1) + 1
		//startTime := time.Now()
		//CountingSort(sl1, k)
		//elapsedTime := time.Since(startTime)
		//fmt.Printf("Counting Sort took: %s\n", elapsedTime)
		wg.Done()
	}()

	go func() {
		sl2 := make([]int, 1000000000)
		for i := 0; i < 1000000000; i++ {
			num := rand.Int()
			sl2[i] = num
		}

		startTime := time.Now()
		QuickSort(sl2)
		elapsedTime := time.Since(startTime)
		fmt.Printf("Quick Sort took: %s\n", elapsedTime)
		wg.Done()
	}()

	go func() {
		sl3 := make([]int, 1000000000)
		for i := 0; i < 1000000000; i++ {
			num := rand.Int()
			sl3[i] = num
		}

		startTime := time.Now()
		RadixSort(sl3)
		elapsedTime := time.Since(startTime)
		fmt.Printf("Radix Sort took: %s\n", elapsedTime)
		wg.Done()
	}()
	wg.Wait()
}

func findMax(arr []int) int {
	if len(arr) == 0 {
		panic("Пустой массив")
	}

	max := arr[0]
	for i := 1; i < len(arr); i++ {
		if arr[i] > max {
			max = arr[i]
		}
	}

	return max
}

func CountingSort(arr []int, k int) {
	c := make([]int, k)
	for i := 0; i < len(arr); i++ {
		c[arr[i]]++
	}
	// pre-fix sum
	for i, sum := 0, 0; i < k; i++ {
		// tmp := c[i]
		// c[i] = sum
		// sum += tmp
		sum, c[i] = sum+c[i], sum
	}
	sorted := make([]int, len(arr))
	for _, n := range arr {
		sorted[c[n]] = n
		c[n]++
	}
	copy(arr, sorted)
}

func RadixSort(array []int) []int {
	// Base 10 is used
	largestNum := findMax(array)
	size := len(array)
	significantDigit := 1
	semiSorted := make([]int, size, size)

	// Loop until we reach the largest significant digit
	for largestNum/significantDigit > 0 {

		bucket := [10]int{0}

		// Counts the number of "keys" or digits that will go into each bucket
		for i := 0; i < size; i++ {
			bucket[(array[i]/significantDigit)%10]++
		}

		// Add the count of the previous buckets
		// Acquires the indexes after the end of each bucket location in the array
		// Works similar to the count sort algorithm
		for i := 1; i < 10; i++ {
			bucket[i] += bucket[i-1]
		}

		// Use the bucket to fill a "semiSorted" array
		for i := size - 1; i >= 0; i-- {
			bucket[(array[i]/significantDigit)%10]--
			semiSorted[bucket[(array[i]/significantDigit)%10]] = array[i]
		}

		// Replace the current array with the semisorted array
		for i := 0; i < size; i++ {
			array[i] = semiSorted[i]
		}

		// Move to next significant digit
		significantDigit *= 10
	}

	return array
}

func QuickSort(arr []int) {
	quickSort(arr, 0, len(arr)-1)
}

func quickSort(arr []int, lo, hi int) {
	if lo < hi {
		pivot := partition(arr, lo, hi)
		quickSort(arr, lo, pivot-1)
		quickSort(arr, pivot+1, hi)
	}
}

// Lomuto partition scheme
func partition(arr []int, lo, hi int) int {
	pivot := arr[hi]
	i := lo
	for j := lo; j < hi; j++ {
		if arr[j] <= pivot {
			arr[i], arr[j] = arr[j], arr[i]
			i++
		}
	}
	arr[i], arr[hi] = arr[hi], arr[i]
	return i
}
