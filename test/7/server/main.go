package main

import (
	"fmt"
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type SubtractService struct{}

type SubtractArgs struct {
	Minuend    int `json:"minuend"`
	Subtrahend int `json:"subtrahend"`
}

type SubtractResult struct {
	Difference int `json:"difference"`
}

func (t *SubtractService) Subtract(args *SubtractArgs, result *SubtractResult) error {
	result.Difference = args.Minuend - args.Subtrahend
	return nil
}

func main() {
	server := rpc.NewServer()
	server.Register(&SubtractService{})
	port := 1234
	l, e := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if e != nil {
		log.Fatal("listen error:", e)
	}
	log.Println(fmt.Sprintf("Server started on port %d", port))
	for {
		conn, err := l.Accept()
		if err != nil {
			log.Fatal(err)
		}
		go server.ServeCodec(jsonrpc.NewServerCodec(conn))
	}
}
