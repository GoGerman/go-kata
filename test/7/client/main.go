// JSON RPC client
package main

import (
	"fmt"
	"log"
	"net/rpc"
	"net/rpc/jsonrpc"
)

type SubtractArgs struct {
	Minuend    int `json:"minuend"`
	Subtrahend int `json:"subtrahend"`
}

type SubtractResult struct {
	Difference int `json:"difference"`
}

type Subtracter interface {
	Subtract(args *SubtractArgs, result *SubtractResult) error
}

type SubtractService struct {
	*rpc.Client
}

type Config struct {
	Host string `json:"host"`
	Port int    `json:"port"`
}

func NewSubtractClient(config Config) (Subtracter, error) {
	client, err := jsonrpc.Dial("tcp", fmt.Sprintf("%s:%d", config.Host, config.Port))
	if err != nil {
		return nil, err
	}
	return &SubtractService{client}, nil
}

func (c *SubtractService) Subtract(args *SubtractArgs, result *SubtractResult) error {
	return c.Client.Call("SubtractService.Subtract", args, result)
}

func main() {
	client, err := NewSubtractClient(Config{
		Host: "localhost",
		Port: 1234,
	})
	if err != nil {
		log.Fatal("dialing:", err)
	}
	args := &SubtractArgs{42, 23}
	var result SubtractResult
	err = client.Subtract(args, &result)
	if err != nil {
		log.Fatal("T.Subtract error:", err)
	}
	log.Printf("Subtract: %d\n", result.Difference)
}
