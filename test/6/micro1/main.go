package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-chi/chi"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"
)

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

type UserServicer interface {
	GetUserByID(id int) (User, error)
	CreateUser(user User)
}

type UserService struct {
	users []User
}

func NewUserService() UserServicer {
	return &UserService{}
}

func (s *UserService) CreateUser(user User) {
	s.users = append(s.users, user)
}

func (s *UserService) GetUserByID(id int) (User, error) {
	for _, user := range s.users {
		if user.ID == id {
			return user, nil
		}
	}
	return User{}, errors.New("user not found, но я тут")
}

type UserHandler struct {
	hand UserServicer
}

func NewUserHandler(serv UserServicer) *UserHandler {
	return &UserHandler{
		hand: serv,
	}
}

func (u *UserHandler) CreateUserHandler(w http.ResponseWriter, r *http.Request) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		// Обработка ошибки декодирования JSON
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Создание пользователя в сервисе пользователя
	u.hand.CreateUser(user)

	// Возвращение успешного ответа
	w.WriteHeader(http.StatusCreated)
}

func (u *UserHandler) GetUserByIDHandler(w http.ResponseWriter, r *http.Request) {
	// Извлечение идентификатора пользователя из URL или параметров запроса
	userID := chi.URLParam(r, "id") // ...
	id, err := strconv.Atoi(userID)
	if err != nil {
		// Обработка ошибки
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Получение пользователя по идентификатору
	user, err := u.hand.GetUserByID(id)
	if err != nil {
		// Обработка ошибки
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	// Возвращение пользователя в качестве ответа
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		return
	}
}

func main() {
	// Создание роутера
	router := chi.NewRouter()

	serv := NewUserService()
	hand := NewUserHandler(serv)
	// Назначение обработчиков для маршрутов
	router.Post("/users", hand.CreateUserHandler)
	router.Get("/users/{id}", hand.GetUserByIDHandler)

	// Запуск сервера

	srv := &http.Server{
		Addr:    ":8080",
		Handler: router,
	}

	go func() {
		log.Println(fmt.Sprintf("server started on port :8080"))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
	log.Fatal(http.ListenAndServe("localhost:8000", router))
}
