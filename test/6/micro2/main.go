package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
)

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func main() {
	user, err := FetchUserByID(1)
	fmt.Println(user, err)
}

func FetchUserByID(userID int) (User, error) {
	url := "http://localhost:8080/users/" + strconv.Itoa(userID)
	resp, err := http.Get(url)
	if err != nil {
		// Обработка ошибки HTTP запроса
		return User{}, err
	}
	defer resp.Body.Close()
	body, _ := io.ReadAll(resp.Body)
	fmt.Println(string(body))
	if resp.StatusCode != http.StatusOK {
		// Обработка ошибки HTTP кода состояния
		return User{}, fmt.Errorf("failed to fetch user. StatusCode: %d", resp.StatusCode)
	}

	var user User
	err = json.NewDecoder(resp.Body).Decode(&user)
	if err != nil {
		// Обработка ошибки декодирования JSON
		return User{}, err
	}

	return user, nil
}
