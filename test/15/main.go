package main

import (
	"fmt"
)

func main() {
	//sl := []int{15: 100, 20: 1111, 3, 4, 5}
	sl := make([]int, 1, 10)
	//	fmt.Println(sl[1])
	var sl1 []int
	fmt.Println(sl == nil)
	fmt.Println(sl1 == nil)
	fmt.Println(sl) // до
	ch(sl)
	var intf interface{}
	_ = intf

	fmt.Println(sl) // после
	sl = sl[:2]
	fmt.Println(sl)
	//panic("sss")
	//fmt.Println("1")

	a := [5]int{1, 2, 3, 4, 5}
	t := a[3:4:4]
	fmt.Println(cap(t))

	var x []int
	x = append(x, 0)
	x = append(x, 1)
	x = append(x, 2)
	y := append(x, 3)
	fmt.Println(x)
	z := append(x, 4)
	fmt.Println(y, z)
}

func ch(sl []int) {
	//A:
	//fmt.Println("1111")
	//defer func() {
	//	if r := recover(); r != nil {
	//		fmt.Println("xuy", r)
	//	}
	//}()
	//sl[0] = 12
	//goto A
	sl = append(sl, 120)
}
