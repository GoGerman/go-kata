module gitlab.com/go-kata/test/12

go 1.20

require (
	github.com/emersion/go-sasl v0.0.0-20220912192320-0145f2c60ead
	github.com/emersion/go-smtp v0.17.0
)
