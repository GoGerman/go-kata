//package main
//
//import (
//	"fmt"
//	"math/rand"
//	"time"
//)
//
////func main() {
////chaan := make(chan interface{})
////done := make(chan struct{})
////done1 := make(chan struct{})
////
////go func() {
////	for i := 0; i < 10; i++ {
////		chaan <- strconv.Itoa(i)
////	}
////	//done <- struct{}{}
////}()
////
////go func() {
////	for i := 0; i < 10; i++ {
////		n := <-chaan
////		d := n.(string)
////		fmt.Println(d + strconv.Itoa(i))
////	}
////	done1 <- struct{}{}
////}()
////
//////	<-done
////<-done1
////
////close(chaan)
////close(done)
////}
//
//func asChan(vs ...int) <-chan int {
//	c := make(chan int)
//
//	go func() {
//		for _, v := range vs {
//			c <- v
//			time.Sleep(time.Duration(rand.Intn(1000)) * time.Millisecond)
//		}
//
//		close(c)
//	}()
//	return c
//}
//
//func merge(a, b <-chan int) <-chan int {
//	c := make(chan int)
//	d := make(chan struct{})
//	defer close(c)
//	go func() {
//		for {
//			select {
//			case v := <-a:
//				c <- v
//			case v := <-b:
//				c <- v
//			}
//		}
//		d <- struct{}{}
//	}()
//
//	return c
//}
//
//func main() {
//
//	a := asChan(1, 3, 5, 7)
//	b := asChan(2, 4, 6, 8)п
//	c := merge(a, b)
//
//	for v := range c {
//		fmt.Println(v)
//	}
//
//	for {
//		func() {
//
//		}()
//	}
//}

package main

import (
	"fmt"
	"sync"
)

func main() {

	// какой будет результат выполнения функции
	ch := make(chan int)

	wg := &sync.WaitGroup{}
	wg.Add(3)

	for i := 0; i < 0; i++ {
		go func(idx int, wg *sync.WaitGroup) {
			ch <- (idx + 1) * 2

			wg.Done()

		}(i, wg)
		fmt.Printf("result: Y%d\n", <-ch)
		wg.Wait()
	}
}
