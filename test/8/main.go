package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"
)

type Coins map[string]QueryValue

type QueryValue struct {
	BuyPrice  string `json:"buy_price" db:"buy_price" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	SellPrice string `json:"sell_price" db:"sell_price" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	LastTrade string `json:"last_trade" db:"last_trade" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	High      string `json:"high" db:"high" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	Low       string `json:"low" db:"low" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	Avg       string `json:"avg" db:"avg" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	Vol       string `json:"vol" db:"vol" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	VolCurr   string `json:"vol_curr" db:"vol_curr" db_ops:"=" db_type:"text" db_default:"" db_index:"true"`
	Updated   int64  `json:"updated" db:"updated" db_ops:"=" db_type:"bigint" db_default:"0" db_index:"true"`
}

func main() {
	ticker := time.NewTicker(2 * time.Second)
	done := make(chan struct{})
	//go func() {
	for {
		select {
		case <-ticker.C:
			response, err := http.Get("https://api.exmo.com/v1.1/ticker")
			if err != nil {
				log.Fatal(err)
			}
			m := make(Coins)
			err = json.NewDecoder(response.Body).Decode(&m)
			if err != nil {
				log.Fatal(err)
			}
			log.Println(m)
		case <-done:
			return
		}
	}
	//}()
}
