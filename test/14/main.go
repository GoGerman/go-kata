package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"sync"
)

func main() {
	ch1 := make(chan int, 1)
	ch2 := make(chan int, 1)
	ch3 := make(chan int, 1)

	var wg sync.WaitGroup
	mch := make(chan int, 1)

	wg.Add(5)

	// Горутина для отправки случайных чисел в канал ch1
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			ch1 <- rand.Intn(100)
		}
		close(ch1)
	}()

	// Горутина для отправки случайных чисел в канал ch2
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			ch2 <- rand.Intn(100)
		}
		close(ch2)
	}()

	// Горутина для отправки случайных чисел в канал ch3
	go func() {
		defer wg.Done()
		for i := 0; i < 10; i++ {
			ch3 <- rand.Intn(100)
		}
		close(ch3)
	}()

	//ctx, cancel := context.WithCancel(context.Background())
	// Горутина-воркер, который умножает числа на два и отправляет в mch
	go func() {
		defer wg.Done()

		out := mergeChannels(ch1, ch2, ch3)
		defer close(mch)
		for {
			select {
			case num, ok := <-out:
				if !ok {
					return
				}
				mch <- num * 2
				//case <-ctx.Done():
				//	return
			}
		}
	}()

	// Главная горутина, которая выводит числа из mch
	go func() {
		defer wg.Done()
		for num := range mch {
			fmt.Println(num)
		}
	}()

	wg.Wait()
	//cancel()
	fmt.Println(runtime.NumGoroutine())
}

func mergeChannels(chs ...chan int) <-chan int {
	out := make(chan int)

	var wg sync.WaitGroup
	wg.Add(len(chs))

	for _, ch := range chs {
		go func(c chan int) {
			defer wg.Done()
			for num := range c {
				out <- num
			}
		}(ch)
	}
	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}
