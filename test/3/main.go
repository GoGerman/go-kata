// You can edit this code!
// Click here and start typing.
package main

import (
	"fmt"
	"math/rand"
	"sort"
	"sync"
)

func main() {
	ch := NewChanWithNumbers(1000)
	// получаем 2 слайса с канала
	slice1, slice2 := ReadFromChannel(ch)
	// вывести 2 слайса
	fmt.Println(slice1)
	fmt.Println(slice2)

	BubbleSort(slice1)
	BubbleSort(slice2)
	// отсортировать слайсы бабл сортом

	// смержить отсортированные слайсы в один отсортированный
	slice3 := MergeSlices(slice1, slice2)

	fmt.Println(slice3)
	// выводим информацию is sorted и сам слайс
	result := IsSorted(slice3)

	fmt.Println(result)
}

func BubbleSort(arr []int) {
	for i := range arr {
		for j := 0; j < len(arr)-i-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
			}
		}
	}
}

func MergeSlices(a []int, b []int) []int {
	// смержить 2 слайса сохраняя сортировку
	var c []int
	c = append(c, b...)
	c = append(c, a...)
	BubbleSort(c)
	return c
}

func IsSorted(arr []int) bool {
	result := sort.SliceIsSorted(arr,
		func(i, j int) bool { return arr[i] < arr[j] })
	return result
}

func ReadFromChannel(ch chan int) ([]int, []int) {
	slice1 := make([]int, 610)
	slice2 := make([]int, 987)

	var wg sync.WaitGroup
	wg.Add(1)
	go func(a, b []int) {
		for i := range a {
			a[i] = <-ch
		}
		for i := range b {
			b[i] = <-ch
		}
		wg.Done()
	}(slice1, slice2)
	wg.Wait()

	return slice1, slice2
}

func NewChanWithNumbers(capacity int) chan int {
	ch := make(chan int, capacity)

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		for i := 0; i < 2000; i++ {
			num := rand.Intn(2000)
			ch <- num
		}
		wg.Wait()
	}()
	wg.Done()

	// Возвращаем буферизированный канал
	return ch

}
