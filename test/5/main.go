package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"io/ioutil"
	"net/http"

	"log"
)

type City struct {
	Id    int    `db:"Id"`
	Name  string `db:"Name"`
	State string `db:"State"`
}

type Credentials struct {
	Password string `json:"password"`
}

type LibInterface interface {
	CreateCities(city City) (City, error)
	GetCities() ([]City, error)
	UpdateCities(city City) error
	DeleteCities(city City) error
}

func main() {
	//http://94.103.95.135:8081/creds

	resp, err := http.Get("http://94.103.95.135:8081/creds")
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)

	creds := Credentials{}
	err = json.Unmarshal(body, &creds)
	if err != nil {
		log.Fatal(err)
	}

	db, err := sqlx.Connect("postgres", "user=postgres dbname=postgres port=8081 host=94.103.95.135 "+
		"password="+creds.Password+"sslmode=disable")
	if err != nil {
		log.Fatalln(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	cre := "CREATE TABLE IF NOT EXISTS cities (id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(30) NOT NULL, state VARCHAR(30) NOT NULL)"
	_, err = db.Exec(cre)
	if err != nil {
		log.Fatalln(err)
	}

	repo := NewLibraryRepository(db)

	c1 := City{
		Id:    1,
		Name:  "asd",
		State: "dsa",
	}

	_, _ = repo.CreateCities(c1)
}

type Repository struct {
	db *sqlx.DB
}

func NewLibraryRepository(db *sqlx.DB) LibInterface {
	return &Repository{
		db: db,
	}
}

func (r *Repository) CreateCities(city City) (City, error) {
	query := "INSERT INTO cities (id, name, state) VALUES (?, ?, ?)"
	_, err := r.db.Exec(query, city.Id, city.Name, city.State)
	if err != nil {
		return City{}, err
	}
	fmt.Println(city)
	return city, nil
}

func (r *Repository) GetCities() ([]City, error) {
	cities := []City{}
	query := "SELECT * FROM cities"
	err := r.db.Select(&cities, query)
	if err != nil {
		log.Println(err)
		return nil, errors.New("can't get cities")
	}
	return cities, nil
}

func (r *Repository) UpdateCities(city City) error {
	query := "UPDATE cities SET name = $1, State = $2 WHERE id = $3"
	_, err := r.db.Exec(query, city.Name, city.State, city.Id)
	if err != nil {
		log.Println(err)
		return errors.New("can't update city")
	}
	return nil
}

func (r *Repository) DeleteCities(city City) error {
	query := "DELETE FROM cities WHERE id = $1"
	_, err := r.db.Exec(query, city.Id)
	if err != nil {
		log.Println(err)
		return errors.New("can't delete city")
	}
	return nil
}
