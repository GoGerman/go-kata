package main

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)

type Valute struct {
	ID       string `xml:"ID,attr"`
	NumCode  string `xml:"NumCode"`
	CharCode string `xml:"CharCode"`
	Nominal  int    `xml:"Nominal"`
	Name     string `xml:"Name"`
	Value    string `xml:"Value"`
}

type ValCurs struct {
	Date      string   `xml:"Date,attr"`
	Name      string   `xml:"Name,attr"`
	ValuteSet []Valute `xml:"Valute"`
}

func getValCurs(url string) (*ValCurs, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	valCurs := &ValCurs{}
	err = xml.Unmarshal(body, valCurs)
	if err != nil {
		return nil, err
	}

	return valCurs, nil
}

func main() {
	endDate := time.Now()
	startDate := endDate.AddDate(0, 0, -90)
	url := fmt.Sprintf("https://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=%s&date_req2=%s&VAL_NM_RQ=R01235",
		startDate.Format("02/01/2006"), endDate.Format("02/01/2006"))
	valCurs, err := getValCurs(url)
	if err != nil {
		panic(err)
	}

	// find max, min, and average values
	maxVal := 0.0
	minVal := 0.0
	sumVal := 0.0
	valMap := make(map[float64]string)
	for _, val := range valCurs.ValuteSet {
		value, err := strconv.ParseFloat(strings.Replace(val.Value, ",", ".", -1), 64)
		if err != nil {
			panic(err)
		}
		sumVal += value
		valMap[value] = val.Name
	}
	sortedVals := make([]float64, 0, len(valMap))
	for val := range valMap {
		sortedVals = append(sortedVals, val)
	}
	sort.Float64s(sortedVals)
	maxVal = sortedVals[len(sortedVals)-1]
	minVal = sortedVals[0]
	avgVal := sumVal / float64(len(valCurs.ValuteSet))

	// find dates of max and min values
	var maxDate string
	var minDate string
	for _, val := range valCurs.ValuteSet {
		value, err := strconv.ParseFloat(strings.Replace(val.Value, ",", ".", -1), 64)
		if err != nil {
			panic(err)
		}
		if value == maxVal {
			maxDate = valCurs.Date
		}
		if value == minVal {
			minDate = valCurs.Date
		}
	}

	// print results
	fmt.Printf("Максимальный курс валюты: %s (%.4f) на дату %s\n", valMap[maxVal], maxVal, maxDate)
	_, _ = avgVal, minDate
}
