package main

import "fmt"

type ProductA interface {
	UseA() string
}
type ProductB interface {
	UseB() string
}

type AbstractFactory interface {
	CreateProductA() ProductA
	CreateProductB() ProductB
}

type ConcreteProductA1 struct{}

func (p ConcreteProductA1) UseA() string {
	return "ConcreteProductA1"
}

type ConcreteProductA2 struct{}

func (p ConcreteProductA2) UseA() string {
	return "ConcreteProductA2"
}

type ConcreteProductB1 struct{}

func (p ConcreteProductB1) UseB() string {
	return "ConcreteProductB1"
}

type ConcreteProductB2 struct{}

func (p ConcreteProductB2) UseB() string {
	return "ConcreteProductB2"
}

type ConcreteFactory1 struct{}

func (f ConcreteFactory1) CreateProductA() ProductA {
	return ConcreteProductA1{}
}
func (f ConcreteFactory1) CreateProductB() ProductB {
	return ConcreteProductB1{}
}

type ConcreteFactory2 struct{}

func (f ConcreteFactory2) CreateProductA() ProductA {
	return ConcreteProductA2{}
}
func (f ConcreteFactory2) CreateProductB() ProductB {
	return ConcreteProductB2{}
}

func main() {
	productA1 := ConcreteFactory1{}.CreateProductA()
	productB1 := ConcreteFactory1{}.CreateProductB()

	fmt.Println(productA1.UseA())
	fmt.Println(productB1.UseB())

	productA2 := ConcreteFactory2{}.CreateProductA()
	productB2 := ConcreteFactory2{}.CreateProductB()

	fmt.Println(productA2.UseA())
	fmt.Println(productB2.UseB())
}
