package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

type Worker struct {
	ID int
}

func (w *Worker) DoWork(taskID int) {
	fmt.Printf("Worker %d is processing task %d\n", w.ID, taskID)
	time.Sleep(time.Second * time.Duration(rand.Intn(10)))
	fmt.Printf("Worker %d is done task %d\n", w.ID, taskID)
}

type WorkerPool struct {
	mu      sync.Mutex
	workers []*Worker
}

func NewWorkerPool(poolSize int) *WorkerPool {
	pool := &WorkerPool{}
	for i := 0; i < poolSize; i++ {
		pool.workers = append(pool.workers, &Worker{ID: i})
	}
	return pool
}

func (p *WorkerPool) GetWorker() *Worker {
	p.mu.Lock()
	defer p.mu.Unlock()

	if len(p.workers) == 0 {
		return nil
	}

	worker := p.workers[0]
	p.workers = p.workers[1:]
	return worker
}

func (p *WorkerPool) ReturnWorker(worker *Worker) {
	p.mu.Lock()
	defer p.mu.Unlock()

	p.workers = append(p.workers, worker)
}

func main() {
	const numTasks = 10
	const poolSize = 3

	var wg sync.WaitGroup
	workerPool := NewWorkerPool(poolSize)

	for i := 0; i < numTasks; i++ {
		wg.Add(1)
		go func(taskID int) {
			defer wg.Done()
			for {
				worker := workerPool.GetWorker()
				if worker != nil {
					defer workerPool.ReturnWorker(worker)
					worker.DoWork(taskID)
					break
				} else {
					fmt.Printf("No available workers for task %d\n", taskID)
					time.Sleep(time.Second * 5)
				}
			}
		}(i)
	}

	wg.Wait()
}
