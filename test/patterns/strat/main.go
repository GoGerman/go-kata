package main

import "fmt"

// Интерфейс стратегии
type PaymentStrategy interface {
	Pay(amount float64)
}

// Конкретная стратегия: оплата кредитной картой
type CreditCardPayment struct {
	cardNumber      string
	cardHolderName  string
	expirationMonth int
	expirationYear  int
	cvv             string
}

func (c *CreditCardPayment) Pay(amount float64) {
	fmt.Printf("Paid %.2f using credit card %s\n", amount, c.cardNumber)
}

// Конкретная стратегия: оплата через PayPal
type PayPalPayment struct {
	email string
}

func (p *PayPalPayment) Pay(amount float64) {
	fmt.Printf("Paid %.2f using PayPal account %s\n", amount, p.email)
}

// Контекст, использующий стратегию
type ShoppingCart struct {
	paymentStrategy PaymentStrategy
}

func (cart *ShoppingCart) SetPaymentStrategy(strategy PaymentStrategy) {
	cart.paymentStrategy = strategy
}

func (cart *ShoppingCart) Checkout(amount float64) {
	if cart.paymentStrategy == nil {
		fmt.Println("Please set a payment strategy first")
		return
	}

	cart.paymentStrategy.Pay(amount)
}

func main() {
	creditCard := &CreditCardPayment{
		cardNumber:      "1234 5678 9012 3456",
		cardHolderName:  "John Doe",
		expirationMonth: 12,
		expirationYear:  2025,
		cvv:             "123",
	}

	payPal := &PayPalPayment{
		email: "john@example.com",
	}

	cart := &ShoppingCart{}

	cart.SetPaymentStrategy(creditCard)
	cart.Checkout(100.00)

	cart.SetPaymentStrategy(payPal)
	cart.Checkout(50.00)
}
