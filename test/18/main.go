package main

import "fmt"

func main() {
	sl := make([]int, 1, 2)
	fmt.Println(sl, "до")
	mutate(sl)
	fmt.Println(sl, "после мутации")
	sl = sl[:2]
	fmt.Println(sl, "расширение")

}

func mutate(sl []int) {
	sl[0] = 100
	sl = append(sl, 200)
}
