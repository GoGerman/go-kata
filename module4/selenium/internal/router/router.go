package router

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/modules/handlers"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/modules/repository"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/modules/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var Vacancy = handlers.NewVacancyController(service.NewVacancyService(repository.NewVacancyRepository()))

func RunServer() {
	port := ":8080"

	r := chi.NewRouter()
	r.Use(middleware.Logger)

	//SwaggerUI
	r.Get("/swagger", handlers.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./internal/public"))).ServeHTTP(w, r)
	})

	//Vacancy
	r.Get("/list", Vacancy.List)
	r.Post("/get/{id}", Vacancy.Get)
	r.Post("/delete/{id}", Vacancy.Delete)
	r.Post("/search/{lang}", Vacancy.Search)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	go func() {
		log.Println(fmt.Sprintf("server started on port %s ", port))
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
