package models

import (
	"bytes"
	"encoding/json"
	"errors"
)

type IDVacancy struct {
	ID string `json:"id"`
}

type Vacancy struct {
	Id                 string
	Context            string             `json:"@context"`
	Type               string             `json:"@type"`
	DatePosted         string             `json:"datePosted"`
	Title              string             `json:"title"`
	Description        string             `json:"description"`
	Identifier         Identifier         `json:"identifier"`
	ValidThrough       string             `json:"validThrough"`
	HiringOrganization HiringOrganization `json:"hiringOrganization"`
	JobLocation        *JobLocationUnion  `json:"jobLocation"`
	BaseSalary         *BaseSalary        `json:"baseSalary,omitempty"`
	EmploymentType     string             `json:"employmentType"`
	JobLocationType    *string            `json:"jobLocationType,omitempty"`
}

type BaseSalary struct {
	Type     string `json:"@type"`
	Currency string `json:"currency"`
	Value    Value  `json:"value"`
}

type Value struct {
	Type     string   `json:"@type"`
	MinValue *float64 `json:"minValue,omitempty"`
	MaxValue *float64 `json:"maxValue,omitempty"`
	UnitText string   `json:"unitText"`
}

type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}

type Identifier struct {
	Type  string `json:"@type"`
	Name  string `json:"name"`
	Value string `json:"value"`
}

type JobLocationElement struct {
	Type    string `json:"@type"`
	Address string `json:"address"`
}

type PurpleJobLocation struct {
	Type    string        `json:"@type"`
	Address *AddressUnion `json:"address"`
}

type AddressClass struct {
	Type            string         `json:"@type"`
	StreetAddress   string         `json:"streetAddress"`
	AddressLocality string         `json:"addressLocality"`
	AddressCountry  AddressCountry `json:"addressCountry"`
}

type AddressCountry struct {
	Type string `json:"@type"`
	Name string `json:"name"`
}

type JobLocationUnion struct {
	JobLocationElementArray []JobLocationElement
	PurpleJobLocation       *PurpleJobLocation
}

func (x *JobLocationUnion) UnmarshalJSON(data []byte) error {
	x.JobLocationElementArray = nil
	x.PurpleJobLocation = nil
	var c PurpleJobLocation
	object, err := unmarshalUnion(data, nil, nil, nil, nil, true, &x.JobLocationElementArray, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.PurpleJobLocation = &c
	}
	return nil
}

func (x *JobLocationUnion) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, nil, x.JobLocationElementArray != nil, x.JobLocationElementArray, x.PurpleJobLocation != nil, x.PurpleJobLocation, false, nil, false, nil, false)
}

type AddressUnion struct {
	AddressClass *AddressClass
	String       *string
}

func (x *AddressUnion) UnmarshalJSON(data []byte) error {
	x.AddressClass = nil
	var c AddressClass
	object, err := unmarshalUnion(data, nil, nil, nil, &x.String, false, nil, true, &c, false, nil, false, nil, false)
	if err != nil {
		return err
	}
	if object {
		x.AddressClass = &c
	}
	return nil
}

func (x *AddressUnion) MarshalJSON() ([]byte, error) {
	return marshalUnion(nil, nil, nil, x.String, false, nil, x.AddressClass != nil, x.AddressClass, false, nil, false, nil, false)
}

func unmarshalUnion(data []byte, pi **int64, pf **float64, pb **bool, ps **string, haveArray bool, pa interface{}, haveObject bool, pc interface{}, haveMap bool, pm interface{}, haveEnum bool, pe interface{}, nullable bool) (bool, error) {
	if pi != nil {
		*pi = nil
	}
	if pf != nil {
		*pf = nil
	}
	if pb != nil {
		*pb = nil
	}
	if ps != nil {
		*ps = nil
	}

	dec := json.NewDecoder(bytes.NewReader(data))
	dec.UseNumber()
	tok, err := dec.Token()
	if err != nil {
		return false, err
	}

	switch v := tok.(type) {
	case json.Number:
		if pi != nil {
			i, err := v.Int64()
			if err == nil {
				*pi = &i
				return false, nil
			}
		}
		if pf != nil {
			f, err := v.Float64()
			if err == nil {
				*pf = &f
				return false, nil
			}
			return false, errors.New("Unparsable number")
		}
		return false, errors.New("Union does not contain number")
	case float64:
		return false, errors.New("Decoder should not return float64")
	case bool:
		if pb != nil {
			*pb = &v
			return false, nil
		}
		return false, errors.New("Union does not contain bool")
	case string:
		if haveEnum {
			return false, json.Unmarshal(data, pe)
		}
		if ps != nil {
			*ps = &v
			return false, nil
		}
		return false, errors.New("Union does not contain string")
	case nil:
		if nullable {
			return false, nil
		}
		return false, errors.New("Union does not contain null")
	case json.Delim:
		if v == '{' {
			if haveObject {
				return true, json.Unmarshal(data, pc)
			}
			if haveMap {
				return false, json.Unmarshal(data, pm)
			}
			return false, errors.New("Union does not contain object")
		}
		if v == '[' {
			if haveArray {
				return false, json.Unmarshal(data, pa)
			}
			return false, errors.New("Union does not contain array")
		}
		return false, errors.New("Cannot handle delimiter")
	}
	return false, errors.New("Cannot unmarshal union")

}

func marshalUnion(pi *int64, pf *float64, pb *bool, ps *string, haveArray bool, pa interface{}, haveObject bool, pc interface{}, haveMap bool, pm interface{}, haveEnum bool, pe interface{}, nullable bool) ([]byte, error) {
	if pi != nil {
		return json.Marshal(*pi)
	}
	if pf != nil {
		return json.Marshal(*pf)
	}
	if pb != nil {
		return json.Marshal(*pb)
	}
	if ps != nil {
		return json.Marshal(*ps)
	}
	if haveArray {
		return json.Marshal(pa)
	}
	if haveObject {
		return json.Marshal(pc)
	}
	if haveMap {
		return json.Marshal(pm)
	}
	if haveEnum {
		return json.Marshal(pe)
	}
	if nullable {
		return json.Marshal(nil)
	}
	return nil, errors.New("Union must not be null")
}
