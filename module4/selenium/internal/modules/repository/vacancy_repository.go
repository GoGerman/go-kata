package repository

import (
	"errors"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/models"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/modules/selenium"
	"log"
	"strconv"
)

type Repository interface {
	Search(lang string) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type VacancyRepository struct {
	data map[string]models.Vacancy
}

func NewVacancyRepository() *VacancyRepository {
	return &VacancyRepository{
		data: make(map[string]models.Vacancy),
	}
}

func (v *VacancyRepository) Search(lang string) error {
	list := selenium.FirefoxDriverAndParser(lang)
	for i := range list {
		list[i].Id = strconv.Itoa(i)
		v.data[strconv.Itoa(i)] = list[i]
	}
	return nil
}

func (v *VacancyRepository) GetById(id int) (models.Vacancy, error) {
	if _, ok := v.data[strconv.Itoa(id)]; !ok {
		return v.data[strconv.Itoa(id)], nil
	} else {
		return models.Vacancy{}, errors.New("id not found")
	}
}

func (v *VacancyRepository) GetList() ([]models.Vacancy, error) {
	var vacancies []models.Vacancy
	for _, vacancy := range v.data {
		vacancies = append(vacancies, vacancy)
	}
	log.Printf("Общее количество вакансий: %d", len(v.data))
	return vacancies, nil
}

func (v *VacancyRepository) Delete(id int) error {
	if _, ok := v.data[strconv.Itoa(id)]; !ok {
		delete(v.data, strconv.Itoa(id))
		return nil
	} else {
		return errors.New("id not found")
	}
}
