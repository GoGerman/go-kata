package service

import (
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/models"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/modules/repository"
)

type Service interface {
	SearchVacancy(lang string) error
	GetByIdVacancy(id int) (models.Vacancy, error)
	GetListVacancies() ([]models.Vacancy, error)
	DeleteVacancy(id int) error
}

type VacancyService struct {
	Data *repository.VacancyRepository
}

func NewVacancyService(data *repository.VacancyRepository) *VacancyService {
	return &VacancyService{Data: data}
}

func (v *VacancyService) SearchVacancy(lang string) error {
	return v.Data.Search(lang)
}

func (v *VacancyService) GetByIdVacancy(id int) (models.Vacancy, error) {
	return v.Data.GetById(id)
}

func (v *VacancyService) GetListVacancies() ([]models.Vacancy, error) {
	return v.Data.GetList()
}

func (v *VacancyService) DeleteVacancy(id int) error {
	return v.Data.Delete(id)
}
