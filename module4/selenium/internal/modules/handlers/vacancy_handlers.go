package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/modules/service"
	"net/http"
	"strconv"
)

type VacancyController struct {
	Data *service.VacancyService
}

func NewVacancyController(data *service.VacancyService) *VacancyController {
	return &VacancyController{Data: data}
}

func (v *VacancyController) List(w http.ResponseWriter, r *http.Request) {
	list, err := v.Data.GetListVacancies()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(list)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) Get(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	num, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	data, err := v.Data.GetByIdVacancy(num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(data)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyController) Delete(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "id")

	num, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = v.Data.DeleteVacancy(num)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
}

func (v *VacancyController) Search(w http.ResponseWriter, r *http.Request) {
	lang := chi.URLParam(r, "lang")

	err := v.Data.SearchVacancy(lang)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	err = json.NewEncoder(w).Encode(err)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
