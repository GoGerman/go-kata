package selenium

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/models"
	"log"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	driverPort = 4444
)

var Wg = sync.WaitGroup{}
var Mu = sync.Mutex{}

func FirefoxDriverAndParser(lang string) []models.Vacancy {
	log.Println("Start parsing")

	caps := selenium.Capabilities{"browserName": "firefox"}

	var wd selenium.WebDriver // Создаем новый драйвер с заданными настройками
	var err error
	for i := 0; i < 5; i++ {
		wd, err = selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", driverPort)) // Соединяемся с драйвером
		if err == nil {
			log.Println("Connecting successful")
			break
		}
		log.Printf("Error connecting to remote driver: %v", err)
		time.Sleep(30 * time.Second)
	}

	if wd == nil {
		log.Fatalf("Failed to connect to remote driver after 5 attempts")
	}
	defer wd.Quit()

	// Сразу обращаемся к странице с поиском вакансии по запросу
	page := 1 // номер страницы
	err = wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, lang))
	if err != nil {
		log.Fatalln(err)
	}

	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		log.Fatalln(err)
	}

	vacancyCountRow, err := elem.Text()
	if err != nil {
		log.Fatalln(err)
	}

	vacancyCount, err := strconv.Atoi(strings.Fields(vacancyCountRow)[1])
	if err != nil {
		log.Fatalln(err)
	}

	countOfPage := vacancyCount / 25
	if countOfPage%25 > 0 {
		countOfPage++
	}

	list := make([]models.Vacancy, vacancyCount)
	var countVacancy, errUnmarhal int
	for i := 1; i <= countOfPage; i++ {
		Wg.Add(1)
		go func(i int, lang string) {
			wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, lang))

			elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
			if err != nil {
				log.Fatalln(err)
			}

			for i := range elems {
				link, err := elems[i].GetAttribute("href")
				log.Printf("Вакансия найдена: %s\n", link)
				if err != nil {
					continue
				}

				resp, err := http.Get("https://career.habr.com" + link)
				if err != nil {
					log.Println(err)
				}
				var doc *goquery.Document
				doc, err = goquery.NewDocumentFromReader(resp.Body)
				if err != nil && doc != nil {
					log.Println(err)
				}
				dd := doc.Find("script[type=\"application/ld+json\"]")
				if dd == nil {
					log.Fatalln("habr vacancy nodes not found")
				}

				var vacancy models.Vacancy
				b := bytes.NewBufferString(dd.First().Text())
				err = json.Unmarshal(b.Bytes(), &vacancy)
				if err != nil {
					log.Println(err)
					errUnmarhal++
				}
				Mu.Lock()
				list = append(list, vacancy)
				Mu.Unlock()
				countVacancy++
			}
			Wg.Done()
		}(i, lang)
	}
	Wg.Wait()

	fmt.Printf("Количество вакансий: %d\nКоличество ошибок: %d\n", countVacancy, errUnmarhal)
	log.Println("Done parsing")
	return list
}
