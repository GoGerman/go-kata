package docs

import "gitlab.com/GoGerman/go-kata/module4/selenium/internal/models"

//go:generate swagger generate spec -o ../public/swagger.json --scan-models

// swagger:route POST /search vacancy vacancySearchRequest
// Создание вакансии.
// responses:
//   200: vacancyCreateResponse

// swagger:parameters vacancySearchRequest
type vacancySearchRequest struct {
	// in:body
	Body models.Query
}

// swagger:response vacancySearchResponse
type vacancySearchResponse struct {
	// in:body
	Body models.Vacancy
}

// swagger:route POST /get vacancy vacancyIdGetRequest
// Поиск вакансии по ID.
// responses:
//   200: vacancyIdGetResponse

// swagger:parameters vacancyIdGetRequest
type vacancyIdGetRequest struct {
	// in:body
	Body models.IDVacancy
}

// swagger:response vacancyIdGetResponse
type vacancyIdGetResponse struct {
	// in:body
	Body models.Vacancy
}

// swagger:route POST /delete vacancy vacancyDeleteRequest
// Удалить вакансию.
// responses:
//   200: vacancyDeleteResponse

// swagger:parameters vacancyDeleteRequest
type vacancyDeleteRequest struct {
	// in:body
	Body models.IDVacancy
}

// swagger:response vacancyDeleteResponse
type vacancyDeleteResponse struct {
	// in:body
	Body models.Vacancy
}

// swagger:route GET /list vacancy vacancyListRequest
// Список всех вакансий
// responses:
//   200: vacancyListResponse

// swagger:parameters vacancyListRequest
type vacancyListRequest struct {
}

// swagger:response vacancyListResponse
type vacancyListResponse struct {
	// in:body
	Body []models.Vacancy
}
