package main

import (
	"gitlab.com/GoGerman/go-kata/module4/selenium/cmd/app"
)

func main() {
	app.Run()
}
