package app

import (
	"gitlab.com/GoGerman/go-kata/module4/selenium/internal/router"
)

func Run() {
	router.RunServer()
}
