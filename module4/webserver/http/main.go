package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Welcome struct {
	Message string `json:"message"`
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	j, err := json.MarshalIndent(Welcome{
		Message: "Hello",
	}, "", "\t")
	if err != nil {
		log.Println(err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(j))

}
