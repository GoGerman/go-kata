module gitlab/go-kata/module4/webserver/http/homework

go 1.20

require (
	github.com/go-chi/chi/v5 v5.0.8
	github.com/mattn/go-sqlite3 v1.14.16
	go.uber.org/zap v1.24.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
