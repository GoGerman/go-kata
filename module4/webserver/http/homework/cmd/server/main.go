package main

import "gitlab/go-kata/module4/webserver/http/homework/internal/app"

func main() {
	app.Run()
}
