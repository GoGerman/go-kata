package logger

import (
	"fmt"
	"go.uber.org/zap"
	"time"
)

func LoggerStart() *zap.SugaredLogger {
	logger, _ := zap.NewProduction()
	sugar := logger.Sugar()
	return sugar
}

func LoggerStop(logger *zap.SugaredLogger) {
	defer logger.Sync()
}

func LoggerForService(level, problem, comment, request, port string, Logger *zap.SugaredLogger) {
	Logger.Infow("",
		"path", level,
		"problem", problem,
		"comment", comment,
		"request", request,
		"port", port,
		"time", fmt.Sprintln(time.Now()),
	)
}
