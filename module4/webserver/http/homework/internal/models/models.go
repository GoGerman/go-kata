package models

type User struct {
	ID        int    `json:"id"`
	FirstName string `json:"name"`
	LastName  string `json:"lastName"`
	Age       int    `json:"age"`
}

type Welcome struct {
	Message string `json:"message"`
}
