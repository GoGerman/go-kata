package repositories

import (
	"database/sql"
	"gitlab/go-kata/module4/webserver/http/homework/internal/models"
	"io"
	"net/http"
	"os"
)

type UserRepository struct {
	db *sql.DB
}

type Users interface {
	GetAll() ([]models.User, error)
	GetID(id int) (models.User, error)
	Upload(r *http.Request, filename string) error
	Show(w http.ResponseWriter, filename string) error
}

func NewUserRepository(db *sql.DB) *UserRepository {
	return &UserRepository{db}
}

func (repo *UserRepository) GetID(id int) (models.User, error) {
	user := models.User{}
	err := repo.db.QueryRow("SELECT * FROM users WHERE id = ?", id).Scan(&user.ID, &user.FirstName, &user.LastName, &user.Age)
	if err != nil {
		return models.User{}, err
	}
	return user, nil
}

func (repo *UserRepository) GetAll() ([]models.User, error) {
	rows, err := repo.db.Query("SELECT * FROM users")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var users []models.User
	for rows.Next() {
		user := models.User{}
		err := rows.Scan(&user.ID, &user.FirstName, &user.LastName, &user.Age)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

func (repo *UserRepository) Upload(r *http.Request, filename string) error {
	file, _, err := r.FormFile("file")
	if err != nil {
		return err
	}
	defer file.Close()

	out, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer out.Close()

	_, err = io.Copy(out, file)
	if err != nil {
		return err
	}

	return nil
}

func (repo *UserRepository) Show(w http.ResponseWriter, filename string) error {
	file, err := os.Open("/Users/pusichkagerman/go/src/gitlab/go-kata/module4/webserver/http/" +
		"homework/internal/public/" + filename)
	defer file.Close()

	_, err = io.Copy(w, file)
	if err != nil {
		return err
	}
	return nil
}
