package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	logs "gitlab/go-kata/module4/webserver/http/homework/internal/logger"
	"gitlab/go-kata/module4/webserver/http/homework/internal/models"
	"gitlab/go-kata/module4/webserver/http/homework/internal/services"
	"log"
	"net/http"
	"strconv"
	"strings"
)

var Logger = logs.LoggerStart()

type Server struct {
	HTTPServer *http.Server
	Service    services.Services
}

func NewServer(s services.Services) *Server {
	return &Server{Service: s}
}

func (s *Server) NewServ() {
	serv := chi.NewRouter()
	port := ":8080"
	s.HTTPServer = &http.Server{
		Addr:    port,
		Handler: serv,
	}

	go func() {
		logs.LoggerForService("server-NewServer", "-",
			"Server started", "-", port, Logger)
		if err := s.HTTPServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	serv.Route("/", func(serv chi.Router) {
		serv.Get("/", Welcome)
		serv.Get("/users", s.GetUsers)
		serv.Get("/users/{ID}", s.GetUserId)
		serv.Post("/upload/{FileName}", s.UploadFile)
		serv.Get("/public/{FileName}", s.GetFile)
	})

	logs.LoggerStop(Logger)
}

func Welcome(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content", "greeting/json")
	logs.LoggerForService("server-Welcome", "-",
		"-", r.Method, fmt.Sprint(r.URL), Logger)
	j, err := json.Marshal(models.Welcome{Message: "Hello user"})
	if err != nil {
		logs.LoggerForService("server-Welcome", "Can't marshal",
			"-", r.Method, fmt.Sprint(r.URL), Logger)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(j))
}

func (s *Server) GetUsers(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content", "users/json")
	logs.LoggerForService("server-GetUsers", "-",
		"-", r.Method, fmt.Sprint(r.URL), Logger)
	model, _ := s.Service.GetAllUsers()
	data, err := json.Marshal(model)
	if err != nil {
		logs.LoggerForService("server-GetUsers", "Can't marshal",
			"-", r.Method, fmt.Sprint(r.URL), Logger)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(data))
}

func (s *Server) GetUserId(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content", "user-id/json")
	logs.LoggerForService("server-GetUserId", "-",
		"-", r.Method, fmt.Sprint(r.URL), Logger)
	in := chi.URLParam(r, "ID")
	id, _ := strconv.Atoi(in)
	model, _ := s.Service.GetUserByID(id)

	data, err := json.Marshal(model)
	if err != nil {
		logs.LoggerForService("server-GetUserId", "Can't marshal",
			"-", r.Method, fmt.Sprint(r.URL), Logger)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(data))
}

func (s *Server) UploadFile(w http.ResponseWriter, r *http.Request) {
	logs.LoggerForService("server-UploadFile", "-",
		"-", r.Method, fmt.Sprint(r.URL), Logger)
	fileName := chi.URLParam(r, "FileName")
	err := s.Service.UploadFile(r, fileName)
	if err != nil {
		logs.LoggerForService("server-UploadFile", fmt.Sprintln(err),
			"-", r.Method, fmt.Sprint(r.URL), Logger)
	} else {
		fmt.Fprint(w, "Файл загружен")
	}
}

func (s *Server) GetFile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content", "get-file/txt")
	logs.LoggerForService("server-GetFile", "-",
		"-", r.Method, fmt.Sprint(r.URL), Logger)

	filename := strings.TrimPrefix(r.URL.Path, "/public/")

	err := s.Service.DownloadFile(w, filename)

	if err != nil {
		logs.LoggerForService("server-GetFile", fmt.Sprintln(err),
			"-", r.Method, fmt.Sprint(r.URL), Logger)
		return
	}
}
