package app

import (
	"context"
	"gitlab/go-kata/module4/webserver/http/homework/internal/services"
	"log"
	"os"
	"os/signal"
	"time"

	"gitlab/go-kata/module4/webserver/http/homework/internal/database"
	"gitlab/go-kata/module4/webserver/http/homework/internal/handlers"
	"gitlab/go-kata/module4/webserver/http/homework/internal/repositories"
)

func Run() {
	db, _ := database.NewDataBase("database")
	users := repositories.NewUserRepository(db)
	//files := repositories.NewFileRepository("./public/")
	servise := services.NewService(users)
	server := handlers.NewServer(servise)
	server.NewServ()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := server.HTTPServer.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
