package database

import (
	"database/sql"
	"encoding/json"
	"gitlab/go-kata/module4/webserver/http/homework/internal/models"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func NewDataBase(name string) (*sql.DB, error) {
	datab, err := sql.Open("sqlite3", name)
	if err != nil {
		log.Fatal("db not open", err)
	}

	prepare, err := datab.Prepare("CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, lastName TEXT, age INTEGER)")
	if err != nil {
		log.Fatal("table not created", err)
	}
	prepare.Exec()

	prepare, err = datab.Prepare("INSERT INTO users (name, lastName, age) VALUES (?, ?, ?)")
	if err != nil {
		log.Fatal(err)
	}

	file, err := os.ReadFile("generated.json")
	var model []models.User
	_ = json.Unmarshal(file, &model)

	for _, v := range model {
		prepare.Exec(v.FirstName, v.LastName, v.Age)
	}

	return datab, nil

}
