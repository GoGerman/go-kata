package services

import (
	"gitlab/go-kata/module4/webserver/http/homework/internal/models"
	"gitlab/go-kata/module4/webserver/http/homework/internal/repositories"
	"net/http"
)

type Services interface {
	DownloadFile(w http.ResponseWriter, fileName string) error
	UploadFile(r *http.Request, fileName string) error
	GetAllUsers() ([]models.User, error)
	GetUserByID(id int) (models.User, error)
}

type Service struct {
	Service repositories.Users
}

func NewService(us repositories.Users) *Service {
	return &Service{Service: us}
}

func (s *Service) DownloadFile(w http.ResponseWriter, fileName string) error {
	return s.Service.Show(w, fileName)
}

func (s *Service) UploadFile(r *http.Request, fileName string) error {
	return s.Service.Upload(r, fileName)
}

func (s *Service) GetAllUsers() ([]models.User, error) {
	return s.Service.GetAll()
}

func (s *Service) GetUserByID(id int) (models.User, error) {
	return s.Service.GetID(id)
}
