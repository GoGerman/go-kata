package repository

import (
	"errors"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"sync"
)

type StoreStorager interface {
	CreateOrder(store entity.Store) entity.Store
	GetOrderByID(order int) entity.Store
	GetInventory() map[string]int
	DeleteOrder(order int) error
}

type StoreStorage struct {
	data               []*entity.Store
	primaryKeyIDx      map[int64]*entity.Store
	autoIncrementCount int64
	sync.Mutex
	file string
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		data:          make([]*entity.Store, 0, 13),
		primaryKeyIDx: make(map[int64]*entity.Store, 13),
	}
}

func (s *StoreStorage) CreateOrder(store entity.Store) entity.Store {
	s.Lock()
	defer s.Unlock()
	store.ID = s.autoIncrementCount
	s.primaryKeyIDx[store.ID] = &store
	s.autoIncrementCount++
	s.data = append(s.data, &store)

	return store
}

func (s *StoreStorage) GetOrderByID(order int) entity.Store {
	if v, ok := s.primaryKeyIDx[int64(order)]; ok {
		return *v
	}
	return entity.Store{}
}

func (s *StoreStorage) GetInventory() map[string]int {
	maps := make(map[string]int)
	var num int
	for _, o := range s.data {
		num = maps[o.Status]
		num++
		maps[o.Status] = num
		num = 0
	}
	return maps
}

func (s *StoreStorage) DeleteOrder(order int) error {
	s.Lock()
	defer s.Unlock()
	if _, ok := s.primaryKeyIDx[int64(order)]; !ok {
		return errors.New("not found")
	}
	delete(s.primaryKeyIDx, int64(order))
	for i, v := range s.data {
		if v.ID == int64(order) {
			s.data = append(s.data[:i], s.data[i+1:]...)
			break
		}
	}
	return nil
}
