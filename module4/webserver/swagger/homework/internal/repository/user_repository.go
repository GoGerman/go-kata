package repository

import (
	"errors"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"sync"
)

type UserStorager interface {
	CreateUser(user entity.User) entity.User
	CreateUserWithArray(users []entity.User) []entity.User
	CreateUserWithList(users []entity.User) []entity.User
	GetUserByUsername(username string) (entity.User, error)
	PutUserByUsername(user entity.User) (entity.User, error)
	DeleteUserByUsername(username string) error
}

type UserStorage struct {
	data               []*entity.User
	primaryKeyIDx      map[int64]*entity.User
	autoIncrementCount int64
	sync.Mutex
	file string
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:          make([]*entity.User, 0, 13),
		primaryKeyIDx: make(map[int64]*entity.User, 13),
	}
}

func (u *UserStorage) CreateUser(user entity.User) entity.User {
	u.Lock()
	defer u.Unlock()
	user.ID = u.autoIncrementCount
	u.primaryKeyIDx[user.ID] = &user
	u.autoIncrementCount++
	u.data = append(u.data, &user)

	return user
}

func (u *UserStorage) CreateUserWithArray(users []entity.User) []entity.User {
	u.Lock()
	defer u.Unlock()
	for i := range users {
		users[i].ID = u.autoIncrementCount
		u.primaryKeyIDx[users[i].ID] = &users[i]
		u.autoIncrementCount++
		u.data = append(u.data, &users[i])
	}

	return users
}

func (u *UserStorage) CreateUserWithList(users []entity.User) []entity.User {
	u.Lock()
	defer u.Unlock()
	for i := range users {
		users[i].ID = u.autoIncrementCount
		u.primaryKeyIDx[users[i].ID] = &users[i]
		u.autoIncrementCount++
		u.data = append(u.data, &users[i])
	}

	return users
}

func (u *UserStorage) GetUserByUsername(username string) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	for i := range u.data {
		if u.data[i].Username == username {
			return *u.data[i], nil
		}
	}
	return entity.User{}, errors.New("user not found")
}

func (u *UserStorage) PutUserByUsername(user entity.User) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyIDx[user.ID]; !ok {
		return entity.User{}, errors.New("not found")
	}
	u.primaryKeyIDx[user.ID] = &user
	for i, v := range u.data {
		if v.ID == user.ID {
			u.data[i] = &user
			break
		}
	}
	return user, nil
}

func (u *UserStorage) DeleteUserByUsername(username string) error {
	u.Lock()
	defer u.Unlock()
	for i, v := range u.data {
		if v.Username == username {
			delete(u.primaryKeyIDx, v.ID)
			u.data = append(u.data[:i], u.data[i+1:]...)
			return nil
		}
	}
	return errors.New("user not found")
}
