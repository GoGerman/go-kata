package repository

import (
	"encoding/json"
	"errors"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"os"
	"sync"
)

type PetStorager interface {
	Create(pet entity.Pet) entity.Pet
	Update(pet entity.Pet) (entity.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (entity.Pet, error)
	GetList() []entity.Pet
	GetByStatus([]string) []entity.Pet
	GetByTags(tags []string) []entity.Pet
	UploadImage(petID int64, name string) error
}

type PetStorage struct {
	data               []*entity.Pet
	primaryKeyIDx      map[int64]*entity.Pet
	autoIncrementCount int64
	sync.Mutex
	file string
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:          make([]*entity.Pet, 0, 13),
		primaryKeyIDx: make(map[int64]*entity.Pet, 13),
	}
}

func (p *PetStorage) Create(pet entity.Pet) entity.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	return pet
}

func (p *PetStorage) Update(pet entity.Pet) (entity.Pet, error) {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[pet.ID]; !ok {
		return entity.Pet{}, errors.New("not found")
	}
	p.primaryKeyIDx[pet.ID] = &pet
	for i, v := range p.data {
		if v.ID == pet.ID {
			p.data[i] = &pet
			break
		}
	}
	return pet, nil
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[int64(petID)]; !ok {
		return errors.New("not found")
	}
	delete(p.primaryKeyIDx, int64(petID))
	for i, v := range p.data {
		if v.ID == int64(petID) {
			p.data = append(p.data[:i], p.data[i+1:]...)
			return nil
		}
	}
	return errors.New("error")
}

func (p *PetStorage) GetByID(petID int) (entity.Pet, error) {
	if v, ok := p.primaryKeyIDx[int64(petID)]; ok {
		return *v, nil
	}
	return entity.Pet{}, errors.New("not found")
}

func (p *PetStorage) GetList() []entity.Pet {
	pets := make([]entity.Pet, len(p.data))
	for i, v := range p.data {
		pets[i] = *v
	}
	return pets
}

func (p *PetStorage) GetByStatus(status []string) []entity.Pet {
	var pets []entity.Pet
	for i := range p.data {
		for _, j := range status {
			if j == p.data[i].Status {
				pets = append(pets, *p.data[i])
			}
		}
	}
	return pets
}

func (p *PetStorage) GetByTags(tags []string) []entity.Pet {
	var pets []entity.Pet

	for _, pet := range p.data {
		for _, tag := range pet.Tags {
			for _, t := range tags {
				if t == tag.Name {
					pets = append(pets, *pet)
					break
				}
			}
		}
	}
	return pets
}

func (p *PetStorage) UploadImage(petID int64, name string) error {
	p.Lock()
	defer p.Unlock()

	pet, err := p.GetByID(int(petID))
	if err != nil {
		return errors.New("fail in GetByID")
	}

	pet.PhotoUrls = append(pet.PhotoUrls, name)

	data, _ := json.MarshalIndent(p.data, "", "\t")
	_ = os.WriteFile(p.file, data, 0766)

	return nil
}
