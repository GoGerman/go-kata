package service

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
)

type StoreServicer interface {
	CreateOrder(store entity.Store) entity.Store
	GetOrderByID(order int) entity.Store
	GetInventory() map[string]int
	DeleteOrder(order int) error
}

type StoreService struct {
	r *repository.StoreStorage
}

func NewStoreService(r *repository.StoreStorage) *StoreService {
	return &StoreService{r: r}
}

func (s *StoreService) CreateOrder(store entity.Store) entity.Store {
	return s.r.CreateOrder(store)
}

func (s *StoreService) GetOrderByID(order int) entity.Store {
	return s.r.GetOrderByID(order)
}

func (s *StoreService) GetInventory() map[string]int {
	return s.r.GetInventory()
}

func (s *StoreService) DeleteOrder(order int) error {
	return s.r.DeleteOrder(order)
}
