package service

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
)

type PetServicer interface {
	UploadImage(petID int64, photoURL string) error
	Create(pet entity.Pet) entity.Pet
	Update(pet entity.Pet) (entity.Pet, error)
	Delete(petID int64)
	GetByID(petID int) (entity.Pet, error)
	GetList() []entity.Pet
	GetByTags([]string) []entity.Pet
	GetByStatus([]string) []entity.Pet
}

type PetService struct {
	r repository.PetStorager
}

func NewPetService(r repository.PetStorager) *PetService {
	return &PetService{r: r}
}

func (p *PetService) UploadImage(petID int64, photoURL string) error {
	err := p.r.UploadImage(petID, photoURL)
	return err
}

func (p *PetService) Create(pet entity.Pet) entity.Pet {
	return p.r.Create(pet)
}

func (p *PetService) Update(pet entity.Pet) (entity.Pet, error) {
	return p.r.Update(pet)
}

func (p *PetService) GetList() []entity.Pet {
	return p.r.GetList()
}

func (p *PetService) GetByStatus(s []string) []entity.Pet {
	return p.r.GetByStatus(s)
}

func (p *PetService) GetByTags(s []string) []entity.Pet {
	return p.r.GetByTags(s)
}

func (p *PetService) GetByID(petID int64) (entity.Pet, error) {
	return p.r.GetByID(int(petID))
}

func (p *PetService) Delete(petID int64) error {
	return p.r.Delete(int(petID))
}
