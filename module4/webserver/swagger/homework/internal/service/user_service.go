package service

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
)

type UserServicer interface {
	CreateUser(user entity.User) entity.User
	CreateUserWithArray(users []entity.User) []entity.User
	CreateUserWithList(users []entity.User) []entity.User
	GetUserByUsername(username string) (entity.User, error)
	PutUserByUsername(user entity.User) (entity.User, error)
	DeleteUserByUsername(username string) error
}

type UserService struct {
	r *repository.UserStorage
}

func NewUserService(r *repository.UserStorage) *UserService {
	return &UserService{r: r}
}

func (u *UserService) CreateUser(user entity.User) entity.User {
	return u.r.CreateUser(user)
}

func (u *UserService) CreateUserWithArray(users []entity.User) []entity.User {
	return u.r.CreateUserWithArray(users)
}

func (u *UserService) CreateUserWithList(users []entity.User) []entity.User {
	return u.r.CreateUserWithList(users)
}

func (u *UserService) GetUserByUsername(username string) (entity.User, error) {
	return u.r.GetUserByUsername(username)
}

func (u *UserService) PutUserByUsername(user entity.User) (entity.User, error) {
	return u.r.PutUserByUsername(user)
}

func (u *UserService) DeleteUserByUsername(username string) error {
	return u.r.DeleteUserByUsername(username)
}
