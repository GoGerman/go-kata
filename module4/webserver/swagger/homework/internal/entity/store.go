package entity

// swagger:model
type Store struct {
	ID       int64  `json:"id"`
	PetID    int64  `json:"petId"`
	Quantity int64  `json:"quantity"`
	ShipDate string `json:"shipDate"`
	Status   string `json:"status"`
	Complete bool   `json:"complete"`
}
