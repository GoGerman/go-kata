package entity

// swagger:model
type ApiResponse struct {
	Code    int64  `json:"code"`
	Type    string `json:"type"`
	Message string `json:"message"`
}
