package testing

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
	"reflect"
	"sync"
	"testing"
)

var UserStorage = repository.NewUserStorage()

func TestUserStorage_CreateUser(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[int64]*entity.User
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		args   entity.User
		want   entity.User
	}{
		{
			name: "first test",
			args: entity.User{
				ID:         0,
				Username:   "0",
				FirstName:  "0",
				LastName:   "0",
				Email:      "0",
				Password:   "0",
				Phone:      "0",
				UserStatus: 0,
			},
			want: entity.User{
				ID:         0,
				Username:   "0",
				FirstName:  "0",
				LastName:   "0",
				Email:      "0",
				Password:   "0",
				Phone:      "0",
				UserStatus: 0,
			},
		},
		{
			name: "second test",
			args: entity.User{
				ID:         1,
				Username:   "1",
				FirstName:  "1",
				LastName:   "1",
				Email:      "1",
				Password:   "1",
				Phone:      "1",
				UserStatus: 1,
			},
			want: entity.User{
				ID:         1,
				Username:   "1",
				FirstName:  "1",
				LastName:   "1",
				Email:      "1",
				Password:   "1",
				Phone:      "1",
				UserStatus: 1,
			},
		},
		{
			name: "third test",
			args: entity.User{
				ID:         2,
				Username:   "2",
				FirstName:  "2",
				LastName:   "2",
				Email:      "2",
				Password:   "2",
				Phone:      "2",
				UserStatus: 2,
			},
			want: entity.User{
				ID:         2,
				Username:   "2",
				FirstName:  "2",
				LastName:   "2",
				Email:      "2",
				Password:   "2",
				Phone:      "2",
				UserStatus: 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage
			if got := u.CreateUser(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateUser() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_CreateUserWithArray(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[int64]*entity.User
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		args   []entity.User
		want   []entity.User
	}{
		{
			name: "first test",
			args: []entity.User{
				{
					ID:         3,
					Username:   "3",
					FirstName:  "3",
					LastName:   "3",
					Email:      "3",
					Password:   "3",
					Phone:      "3",
					UserStatus: 3,
				},
				{
					ID:         4,
					Username:   "4",
					FirstName:  "4",
					LastName:   "4",
					Email:      "4",
					Password:   "4",
					Phone:      "4",
					UserStatus: 4,
				},
				{
					ID:         5,
					Username:   "5",
					FirstName:  "5",
					LastName:   "5",
					Email:      "5",
					Password:   "5",
					Phone:      "5",
					UserStatus: 5,
				},
			},
			want: []entity.User{
				{
					ID:         3,
					Username:   "3",
					FirstName:  "3",
					LastName:   "3",
					Email:      "3",
					Password:   "3",
					Phone:      "3",
					UserStatus: 3,
				},
				{
					ID:         4,
					Username:   "4",
					FirstName:  "4",
					LastName:   "4",
					Email:      "4",
					Password:   "4",
					Phone:      "4",
					UserStatus: 4,
				},
				{
					ID:         5,
					Username:   "5",
					FirstName:  "5",
					LastName:   "5",
					Email:      "5",
					Password:   "5",
					Phone:      "5",
					UserStatus: 5,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage
			if got := u.CreateUserWithArray(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateUserWithArray() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_CreateUserWithList(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[int64]*entity.User
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		args   []entity.User
		want   []entity.User
	}{
		{
			name: "first test",
			args: []entity.User{
				{
					ID:         6,
					Username:   "6",
					FirstName:  "6",
					LastName:   "6",
					Email:      "6",
					Password:   "6",
					Phone:      "6",
					UserStatus: 6,
				},
				{
					ID:         7,
					Username:   "7",
					FirstName:  "7",
					LastName:   "7",
					Email:      "7",
					Password:   "7",
					Phone:      "7",
					UserStatus: 7,
				},
				{
					ID:         8,
					Username:   "8",
					FirstName:  "8",
					LastName:   "8",
					Email:      "8",
					Password:   "8",
					Phone:      "8",
					UserStatus: 8,
				},
			},
			want: []entity.User{
				{
					ID:         6,
					Username:   "6",
					FirstName:  "6",
					LastName:   "6",
					Email:      "6",
					Password:   "6",
					Phone:      "6",
					UserStatus: 6,
				},
				{
					ID:         7,
					Username:   "7",
					FirstName:  "7",
					LastName:   "7",
					Email:      "7",
					Password:   "7",
					Phone:      "7",
					UserStatus: 7,
				},
				{
					ID:         8,
					Username:   "8",
					FirstName:  "8",
					LastName:   "8",
					Email:      "8",
					Password:   "8",
					Phone:      "8",
					UserStatus: 8,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage
			if got := u.CreateUserWithList(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateUserWithList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_GetUserByUsername(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[int64]*entity.User
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    string
		want    entity.User
		wantErr bool
	}{
		{
			name: "first test",
			args: "6",
			want: entity.User{
				ID:         6,
				Username:   "6",
				FirstName:  "6",
				LastName:   "6",
				Email:      "6",
				Password:   "6",
				Phone:      "6",
				UserStatus: 6,
			},
			wantErr: false,
		},
		{
			name: "second test",
			args: "1",
			want: entity.User{
				ID:         1,
				Username:   "1",
				FirstName:  "1",
				LastName:   "1",
				Email:      "1",
				Password:   "1",
				Phone:      "1",
				UserStatus: 1,
			},
			wantErr: false,
		},
		{
			name:    "third test",
			args:    "100",
			want:    entity.User{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage
			got, err := u.GetUserByUsername(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserByUsername() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserByUsername() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_PutUserByUsername(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[int64]*entity.User
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    entity.User
		want    entity.User
		wantErr bool
	}{
		{
			name: "first test",
			args: entity.User{
				ID:         1,
				Username:   "put",
				FirstName:  "put",
				LastName:   "put",
				Email:      "put",
				Password:   "put",
				Phone:      "put",
				UserStatus: 1,
			},
			want: entity.User{
				ID:         1,
				Username:   "put",
				FirstName:  "put",
				LastName:   "put",
				Email:      "put",
				Password:   "put",
				Phone:      "put",
				UserStatus: 1,
			},
			wantErr: false,
		},
		{
			name: "second test",
			args: entity.User{
				ID:         6,
				Username:   "put",
				FirstName:  "put",
				LastName:   "put",
				Email:      "put",
				Password:   "put",
				Phone:      "put",
				UserStatus: 6,
			},
			want: entity.User{
				ID:         6,
				Username:   "put",
				FirstName:  "put",
				LastName:   "put",
				Email:      "put",
				Password:   "put",
				Phone:      "put",
				UserStatus: 6,
			},
			wantErr: false,
		},
		{
			name: "third test",
			args: entity.User{
				ID:         100,
				Username:   "put",
				FirstName:  "put",
				LastName:   "put",
				Email:      "put",
				Password:   "put",
				Phone:      "put",
				UserStatus: 6,
			},
			want:    entity.User{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage
			got, err := u.PutUserByUsername(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("PutUserByUsername() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PutUserByUsername() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_DeleteUserByUsername(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[int64]*entity.User
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    string
		wantErr bool
	}{
		{
			name:    "first test",
			args:    "4",
			wantErr: false,
		},
		{
			name:    "second test",
			args:    "2",
			wantErr: false,
		},
		{
			name:    "third test",
			args:    "50",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := UserStorage
			if err := u.DeleteUserByUsername(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("DeleteUserByUsername() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
