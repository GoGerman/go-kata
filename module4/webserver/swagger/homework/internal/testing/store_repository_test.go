package testing

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
	"reflect"
	"sync"
	"testing"
)

var StoreStorage = repository.NewStoreStorage()

func TestStoreStorage_CreateOrder(t *testing.T) {
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		args   entity.Store
		want   entity.Store
	}{
		{
			name: "first test",
			args: entity.Store{
				ID:       0,
				PetID:    0,
				Quantity: 0,
				ShipDate: "2023-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
			want: entity.Store{
				ID:       0,
				PetID:    0,
				Quantity: 0,
				ShipDate: "2023-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
		},
		{
			name: "second test",
			args: entity.Store{
				ID:       1,
				PetID:    1,
				Quantity: 1,
				ShipDate: "2022-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
			want: entity.Store{
				ID:       1,
				PetID:    1,
				Quantity: 1,
				ShipDate: "2022-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
		},
		{
			name: "third test",
			args: entity.Store{
				ID:       2,
				PetID:    2,
				Quantity: 2,
				ShipDate: "2021-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
			want: entity.Store{
				ID:       2,
				PetID:    2,
				Quantity: 2,
				ShipDate: "2021-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage
			if got := s.CreateOrder(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CreateOrder() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_GetInventory(t *testing.T) {
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]int
	}{
		{
			name: "first test",
			want: map[string]int{
				"placed": 3,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage
			if got := s.GetInventory(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetInventory() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_GetOrderByID(t *testing.T) {
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		args   int
		want   entity.Store
	}{
		{
			name: "first test",
			args: 0,
			want: entity.Store{
				ID:       0,
				PetID:    0,
				Quantity: 0,
				ShipDate: "2023-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
		},
		{
			name: "second test",
			args: 1,
			want: entity.Store{
				ID:       1,
				PetID:    1,
				Quantity: 1,
				ShipDate: "2022-04-28T15:35:53.524Z",
				Status:   "placed",
				Complete: true,
			},
		},
		{
			name: "third test",
			args: 10,
			want: entity.Store{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage
			if got := s.GetOrderByID(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetOrderByID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_DeleteOrder(t *testing.T) {
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    int
		wantErr bool
	}{

		{
			name:    "first test",
			args:    0,
			wantErr: false,
		},
		{
			name:    "second test",
			args:    1,
			wantErr: false,
		},
		{
			name:    "third test",
			args:    10,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := StoreStorage
			if err := s.DeleteOrder(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("DeleteOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
