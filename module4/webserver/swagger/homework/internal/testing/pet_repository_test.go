package testing

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
	"reflect"
	"sync"
	"testing"
)

var PetStorage = repository.NewPetStorage()

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet entity.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: entity.Pet{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
			},
			want: entity.Pet{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags: []entity.Category{
					{
						ID:   0,
						Name: "test tag",
					},
				},
				Status: "active",
			},
		},
		{
			name: "second test",
			args: args{
				pet: entity.Pet{
					ID: 1,
					Category: entity.Category{
						ID:   1,
						Name: "test category",
					},
					Name: "Dima",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
			},
			want: entity.Pet{
				ID: 1,
				Category: entity.Category{
					ID:   1,
					Name: "test category",
				},
				Name: "Dima",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags: []entity.Category{
					{
						ID:   0,
						Name: "test tag",
					},
				},
				Status: "active",
			},
		},
		{
			name: "second test",
			args: args{
				pet: entity.Pet{
					ID: 2,
					Category: entity.Category{
						ID:   2,
						Name: "test category",
					},
					Name: "Liza",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
			want: entity.Pet{
				ID: 2,
				Category: entity.Category{
					ID:   2,
					Name: "test category",
				},
				Name: "Liza",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			var got entity.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(int(got.ID)); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_GetByStatus(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		args   []string
		want   []entity.Pet
	}{
		{
			name: "first test",
			args: []string{"active"},
			want: []entity.Pet{
				{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
				{
					ID: 1,
					Category: entity.Category{
						ID:   1,
						Name: "test category",
					},
					Name: "Dima",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
				{
					ID: 2,
					Category: entity.Category{
						ID:   2,
						Name: "test category",
					},
					Name: "Liza",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			if got := p.GetByStatus(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    int
		want    entity.Pet
		wantErr bool
	}{
		{
			name: "first test",
			args: 0,
			want: entity.Pet{

				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags: []entity.Category{
					{
						ID:   0,
						Name: "test tag",
					},
				},
				Status: "active",
			},
			wantErr: false,
		},
		{
			name: "second test",
			args: 2,
			want: entity.Pet{
				ID: 2,
				Category: entity.Category{
					ID:   2,
					Name: "test category",
				},
				Name: "Liza",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
			wantErr: false,
		},
		{
			name:    "third test",
			args:    10,
			want:    entity.Pet{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			got, err := p.GetByID(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetByTags(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		args   []string
		want   []entity.Pet
	}{
		{
			name: "first test",
			args: []string{"test tag"},
			want: []entity.Pet{
				{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
				{
					ID: 1,
					Category: entity.Category{
						ID:   1,
						Name: "test category",
					},
					Name: "Dima",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			if got := p.GetByTags(tt.args); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByTags() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetList(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name   string
		fields fields
		want   []entity.Pet
	}{
		{
			name: "first test",
			want: []entity.Pet{
				{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
				{
					ID: 1,
					Category: entity.Category{
						ID:   1,
						Name: "test category",
					},
					Name: "Dima",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags: []entity.Category{
						{
							ID:   0,
							Name: "test tag",
						},
					},
					Status: "active",
				},
				{
					ID: 2,
					Category: entity.Category{
						ID:   2,
						Name: "test category",
					},
					Name: "Liza",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			if got := p.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    entity.Pet
		want    entity.Pet
		wantErr bool
	}{
		{
			name: "first test",
			args: entity.Pet{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Kuzia",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags: []entity.Category{
					{
						ID:   0,
						Name: "test tag",
					},
				},
				Status: "passive",
			},
			want: entity.Pet{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Kuzia",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags: []entity.Category{
					{
						ID:   0,
						Name: "test tag",
					},
				},
				Status: "passive",
			},
			wantErr: false,
		},
		{
			name: "second test",
			args: entity.Pet{
				ID: 100,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Kuzia",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags: []entity.Category{
					{
						ID:   0,
						Name: "test tag",
					},
				},
				Status: "passive",
			},
			want:    entity.Pet{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			got, err := p.Update(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
		file               string
	}
	tests := []struct {
		name    string
		fields  fields
		args    int
		wantErr bool
	}{
		{
			name:    "first test",
			args:    0,
			wantErr: false,
		},
		{
			name:    "second test",
			args:    100,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := PetStorage
			if err := p.Delete(tt.args); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
