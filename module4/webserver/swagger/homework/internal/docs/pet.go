package docs

import (
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
)

//go:generate swagger generate spec -o ../public/swagger.json --scan-models

// swagger:route POST /pet pet petCreateRequest
// Создание питомца.
// responses:
//   200: petCreateResponse
//   400: description: Bad request
//	 500: description: Internal server error

// swagger:parameters petCreateRequest
type petCreateRequest struct {
	// required: true
	// in: body
	Pet entity.Pet
}

// swagger:response petCreateResponse
type petCreateResponse struct {
	// in:body
	Pet entity.Pet
}

//swagger:route POST /pet/{petID}/uploadImage pet petUploadImageRequest
// Загрузка изображения питомца по id.
// responses:
//   200: petUploadImageResponse

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petUploadImageResponse
type petUploadImageResponse struct {
	// in:body
	Body entity.ApiResponse
}

// swagger:route PUT /pet pet petUpdateRequest
// Поместить питомца в магазин.
// responses:
//   200: petUpdateResponse

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// In:body
	Body entity.Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet/{petID} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet/findByStatus pet petFindByStatusRequest
// Получение питомцев по статусу.
// responses:
//   200: petFindByStatusResponse

// swagger:parameters petFindByStatusRequest
type petFindByStatusRequest struct {
	// Status питомцев
	// Required: true
	// Enum: available, pending, sold
	// In: query
	Status string `json:"status"`
}

// swagger:response petFindByStatusResponse
type petFindByStatusResponse struct {
	// in:body
	Body []entity.Pet
}

// swagger:route GET /pet/findByTags pet petFindByTagsRequest
// Получение питомца по тэгам.
// responses:
//   200: petFindByTagsResponse

// swagger:parameters petFindByTagsRequest
type petFindByTagsRequest struct {
	// Tags of an item
	// required: true
	// In: path
	Tags string `json:"tags"`
}

// swagger:response petFindByTagsResponse
type petFindByTagsResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route DELETE /pet/{petID} pet petDeleteRequest
// Удаление питамца по ID.
// responses:
//   200: petDeleteResponse

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// ID of pet
	// required: true
	// in:path
	ID string `json:"id"`
}

// swagger:response petDeleteResponse
type petDeleteResponse struct {
	// in:body
	Body entity.ApiResponse
}
