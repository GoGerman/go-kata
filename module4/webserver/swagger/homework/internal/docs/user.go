package docs

import "gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"

//go:generate swagger generate spec -o ../public/swagger.json --scan-models --model-package=entity

// swagger:route POST /user/createWithArray user UsersCreateArrayRequest
// Создание юзеров.
// responses:
//   200: UsersCreateArrayResponse

// swagger:parameters UsersCreateArrayRequest
type UsersCreateArrayRequest struct {
	// in:body
	Body []entity.User
}

// swagger:response UsersCreateArrayResponse
type UsersCreateArrayResponse struct {
	// in:body
	Body entity.ApiResponse
}

// swagger:route POST /user/createWithList user UsersCreateListRequest
// Создание юзеров.
// responses:
//   200: UsersCreateListResponse

// swagger:parameters UsersCreateListRequest
type UsersCreateListRequest struct {
	// in:body
	Body []entity.User
}

// swagger:response UsersCreateListResponse
type UsersCreateListResponse struct {
	// in:body
	Body entity.ApiResponse
}

// swagger:route POST /user user UserCreateRequest
// Создание юзера.
// responses:
//   200: UserCreateResponse

// swagger:parameters UserCreateRequest
type UserCreateRequest struct {
	// in:body
	Body entity.User
}

// swagger:response UserCreateResponse
type UserCreateResponse struct {
	// in:body
	Body entity.ApiResponse
}

// swagger:route GET /user/{username} user GetUserUsernameRequest
// Получение юзера по Username.
// responses:
//   200: GetUserUsernameResponse

// swagger:parameters GetUserUsernameRequest
type GetUserUsernameRequest struct {
	// Username of an item
	//
	// in:body
	Username string `json:"username"`
}

// swagger:response GetUserUsernameResponse
type GetUserUsernameResponse struct {
	// in:body
	Body entity.User
}

// swagger:route PUT /user/{username} user UserUpdateRequest
// Обновление по Username.
// responses:
//   200: UserUpdateResponse

// swagger:parameters UserUpdateRequest
type UserUpdateRequest struct {
	// Username of an item
	//
	// in:body
	Username string `json:"username"`
	Body     entity.User
}

// swagger:response UserUpdateResponse
type UserUpdateResponse struct {
	// in:body
	Body entity.ApiResponse
}

// swagger:route DELETE /user/{username} user UserDeleteRequest
// Удаление юзера по Username.
// responses:
//   200: UserDeleteResponse

// swagger:parameters UserDeleteRequest
type UserDeleteRequest struct {
	// Username of an item
	//
	// in:body
	Username string `json:"username"`
}

// swagger:response UserDeleteResponse
type UserDeleteResponse struct {
	// in:body
	Body entity.ApiResponse
}
