package docs

import "gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"

//go:generate swagger generate spec -o ../public/swagger.json --scan-models --models-package=entity

// swagger:route POST /store/order store PlaceOrderRequest
// Размещение ордера.
// responses:
//   200: PlaceOrderResponse

// swagger:parameters PlaceOrderRequest
type PlaceOrderRequest struct {
	// in:body
	Body entity.Store
}

// swagger:response PlaceOrderResponse
type PlaceOrderResponse struct {
	// in:body
	Body entity.Store
}

// swagger:route GET /store/order/{orderId} store FindOrderIdRequest
// Поиск ордера по ID.
// responses:
//   200: FindOrderIdResponse

// swagger:parameters FindOrderIdRequest
type FindOrderIdRequest struct {
	// ID of an item
	//
	// in:body
	ID int64 `json:"id"`
}

// swagger:response FindOrderIdResponse
type FindOrderIdResponse struct {
	// in:body
	Body entity.Store
}

// swagger:route GET /store/inventory store ReturnsPetInventoriesStatusRequest
// Возвращение инвентаря.
// responses:
//   200: ReturnsPetInventoriesStatusResponse

// swagger:parameters ReturnsPetInventoriesStatusRequest
type ReturnsPetInventoriesStatusRequest struct {
}

// swagger:response ReturnsPetInventoriesStatusResponse
type ReturnsPetInventoriesStatusResponse struct {
	// in:body
	Body entity.Store
}

// swagger:route DELETE /store/order/{orderId} store DeleteOrderIdRequest
// Удаление ордера по ID.
// responses:
//   200: DeleteOrderIdResponse

// swagger:parameters DeleteOrderIdRequest
type DeleteOrderIdRequest struct {
	// ID of an item
	//
	// in:body
	ID int64 `json:"id"`
}

// swagger:response DeleteOrderIdResponse
type DeleteOrderIdResponse struct {
	// in:body
	Body entity.ApiResponse
}
