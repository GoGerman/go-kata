package handlers

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/entity"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/repository"
	"gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/service"
	"net/http"
	"strconv"
)

type StoreController struct { // Body контроллер
	storage service.StoreServicer
}

func NewStoreController() *StoreController {
	return &StoreController{
		storage: service.NewStoreService(repository.NewStoreStorage()),
	}
}

func (s *StoreController) PlaceOrder(w http.ResponseWriter, r *http.Request) {
	var store entity.Store
	err := json.NewDecoder(r.Body).Decode(&store) // считываем приходящий json из *http.Request в структуру Body

	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	store = s.storage.CreateOrder(store) // создаем запись в нашем storage

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(store)                           // записываем результат Body json в http.ResponseWriter

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreController) FindOrderId(w http.ResponseWriter, r *http.Request) {
	orderId := chi.URLParam(r, "orderId") // получаем petID из chi router

	orderID, err := strconv.Atoi(orderId) // конвертируем в int
	if err != nil {                       // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	store := s.storage.GetOrderByID(orderID) // пытаемся получить Body по id

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(store)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreController) DeleteOrderId(w http.ResponseWriter, r *http.Request) {
	orderId := chi.URLParam(r, "orderId") // получаем petID из chi router

	orderID, err := strconv.Atoi(orderId)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = s.storage.DeleteOrder(orderID) // пытаемся получить Body по id
	if err != nil {                      // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(err)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (s *StoreController) ReturnsPetInventoriesStatus(w http.ResponseWriter, r *http.Request) {
	data := s.storage.GetInventory() // пытаемся получить Body по id

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(data)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
