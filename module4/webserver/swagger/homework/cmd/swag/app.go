package swag

import "gitlab.com/GoGerman/go-kata/module4/webserver/swagger/homework/internal/handlers"

func Run() {
	handlers.RunServer()
}
