package main

import (
	"fmt"
	"math/rand"
	"os"
	"time"
)

const (
	noError = iota
	generalError
)

func main() {
	printme("Hello linter")
	rand.Seed(time.Now().UnixNano())
	roll := rand.Intn(100)
	if roll > 50 {
		os.Exit(noError)
	} else {
		os.Exit(generalError)
	}
}

func printme(text string) {
	fmt.Println(text)
}
